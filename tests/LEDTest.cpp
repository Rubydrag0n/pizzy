#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <pizzy/common/Exception.h>
#include <pizzy/core/VirtualPinProvider.h>
#include <pizzy/peripherals/LED.h>

#include <chrono>
#include <thread>

using namespace pizzy;

SCENARIO("constructing led objects on a virtual board", "[led][peripheral]") {

    auto& board = VirtualPinProvider::getInstance();

    auto testPin = GENERATE(take(5, random(1, 20)));

    GIVEN("a default LED") {
        pizzy::peripherals::LED testLED({.pin = board.getPin(testPin)});

        const std::string expectedName =
            std::string("anode-LED at VIRTPIN" + std::to_string(testPin));

        THEN("default values are as expected") {
            REQUIRE(board.getPinState(testPin).currentState == false);
            REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
            REQUIRE(testLED.getState() == false);
            REQUIRE(testLED.getName() == expectedName);
            REQUIRE(testLED.toString() == expectedName + " turned off");
        }
    }

    GIVEN("a LED object") {
        auto defaultState = GENERATE(false, true);
        auto LEDType = GENERATE(peripherals::LEDType::ANODE, peripherals::LEDType::CATHODE);
        const std::string expectedName =
            std::string(LEDType == peripherals::LEDType::ANODE ? "anode" : "cathode") + "-LED at VIRTPIN" + std::to_string(testPin);

        pizzy::peripherals::LED testLED({.pin = board.getPin(testPin), .type = LEDType, .defaultState = defaultState});

        const bool inverted = LEDType == peripherals::LEDType::CATHODE;

        REQUIRE(board.getPinState(testPin).currentState == (defaultState != inverted));
        REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
        REQUIRE(testLED.getState() == defaultState);
        REQUIRE(testLED.getName() == expectedName);
        REQUIRE(testLED.toString() == expectedName + " turned " + (defaultState ? "on" : "off"));

        WHEN("turning LED on") {
            testLED.setState(true);
            THEN("LED is on") {
                REQUIRE(board.getPinState(testPin).currentState == (true != inverted));
                REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
                REQUIRE(testLED.getState() == true);
                REQUIRE(testLED.toString() == expectedName + " turned on");
            }
        }

        WHEN("turning LED off") {
            testLED.setState(false);
            THEN("LED is off") {
                REQUIRE(board.getPinState(testPin).currentState == (false != inverted));
                REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
                REQUIRE(testLED.getState() == false);
                REQUIRE(testLED.toString() == expectedName + " turned off");
            }
        }
    }
}