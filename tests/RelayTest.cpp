#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <pizzy/common/Exception.h>
#include <pizzy/core/VirtualPinProvider.h>
#include <pizzy/peripherals/Relay.h>

#include <chrono>
#include <thread>

using namespace std::chrono_literals;
using namespace pizzy::core;
using namespace pizzy::peripherals;
using namespace pizzy::common;

SCENARIO("constructing relay objects on virtual board", "[relay][peripheral]") {

    auto& board = VirtualPinProvider::getInstance();

    auto testPin = GENERATE(take(2, random(1, 20)));

    const std::string expectedName = "Relay at VIRTPIN" + std::to_string(testPin);

    GIVEN("a default relay") {
        Relay testRelay({.pin = board.getPin(testPin)});

        THEN("has correct values set") {
            REQUIRE(board.getPinState(testPin).currentState == false);
            REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
            REQUIRE(testRelay.getName() == expectedName);
            REQUIRE(testRelay.toString() == expectedName + " turned off");
            REQUIRE(testRelay.getState() == false);
        }
    }

    GIVEN("a relay object") {
        const auto inverted = GENERATE(false, true);
        const auto defaultState = GENERATE(false, true);
        const auto sleepTime = GENERATE(std::chrono::milliseconds(0), std::chrono::milliseconds(500), std::chrono::milliseconds(1000));
        Relay testRelay({.pin = board.getPin(testPin), .defaultState = defaultState, .inverted = inverted, .minTimeBetweenSwitches = sleepTime});

        REQUIRE(board.getPinState(testPin).currentState == (defaultState != inverted));
        REQUIRE(board.getPinState(testPin).pinMode == VirtualPinState::Mode::OUT);
        REQUIRE(testRelay.getName() == expectedName);
        REQUIRE(testRelay.toString() == (expectedName + " turned " + std::string((defaultState ? "on" : "off"))));
        REQUIRE(testRelay.getState() == defaultState);

        WHEN("set on") {
            if (testRelay.getState() != true) {
                std::this_thread::sleep_for(sleepTime);
            }

            REQUIRE_NOTHROW(testRelay.set(true));

            THEN("the relay turns on") {
                REQUIRE(board.getPinState(testPin).currentState == (true != inverted));
                REQUIRE(testRelay.getState() == true);
                REQUIRE(testRelay.toString() == expectedName + " turned on");
            }
        }

        WHEN("set off") {
            if (testRelay.getState() != false) {
                std::this_thread::sleep_for(sleepTime);
            }

            REQUIRE_NOTHROW(testRelay.set(false));

            THEN("the relay turns off") {
                REQUIRE(board.getPinState(testPin).currentState == (false != inverted));
                REQUIRE(testRelay.getState() == false);
                REQUIRE(testRelay.toString() == expectedName + " turned off");
            }
        }

        // can't switch too quickly when there is no minimum time
        if (sleepTime > std::chrono::milliseconds(0)) {
            WHEN("switch too quickly") {
                std::this_thread::sleep_for(sleepTime);
                REQUIRE_NOTHROW(testRelay.set(!defaultState));

                THEN("exception is thrown") {
                    REQUIRE_THROWS_AS(testRelay.set(defaultState), Exception);
                    REQUIRE(board.getPinState(testPin).currentState == (!defaultState != inverted));
                    REQUIRE(testRelay.getState() == !defaultState);
                }
            }
        }

        WHEN("switching slowly") {
            if (testRelay.getState() != true) {
                std::this_thread::sleep_for(sleepTime);
            }
            REQUIRE_NOTHROW(testRelay.set(true));

            THEN("the relay switches as expected") {
                std::this_thread::sleep_for(sleepTime);
                REQUIRE_NOTHROW(testRelay.set(false));
                REQUIRE(board.getPinState(testPin).currentState == (false != inverted));
                REQUIRE(testRelay.getState() == false);
            }
        }
    }
}
