#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <pizzy/common/Exception.h>
#include <pizzy/common/Utility.h>

#include <chrono>
#include <thread>

using namespace pizzy::common;
using namespace std::chrono;
using namespace std::chrono_literals;

SCENARIO("Exceptions", "[utility][exceptions]") {
    const std::string testException = "testexception";
    auto exception = Exception(testException);
    REQUIRE(exception.what() == testException);

    try {
        throw Exception(testException);
        REQUIRE(false);
    }
    catch (const Exception& e) {
        REQUIRE(e.what() == testException);
    }

    REQUIRE_THROWS_AS([&](){throw Exception(testException);}(), Exception);
}

SCENARIO("Utility Functions", "[utility]") {
    REQUIRE(!Utility::getVersion().empty());
}

SCENARIO("Time to String", "[utility]") {
    REQUIRE(Utility::timeToString(50ms) == "50.000ms");
    REQUIRE(Utility::timeToString(99ns) == "0.099us");
    REQUIRE(Utility::timeToString(15'757us) == "15.757ms");
    REQUIRE(Utility::timeToString(385'657ns) == "385.657us");
    REQUIRE(Utility::timeToString(578min + 57s) == "9:38:57");
    REQUIRE(Utility::timeToString(27h) == "27:00:00");
    REQUIRE(Utility::timeToString(124s) == "2:04.000");
    REQUIRE(Utility::timeToString(23'876ms) == "23.876s");
    REQUIRE(Utility::timeToString(1'578'658us) == "1.578s");
}

TEMPLATE_TEST_CASE("Sleep Functions", "[utility][sleep]", int, double) {

    SECTION("microsecond sleep") {
        TestType sleepTime = GENERATE(take(5, random(TestType(100.), TestType(2000.))));

        auto start = clock::now();
        Utility::sleepMicroseconds(sleepTime);
        REQUIRE(duration_cast<microseconds>(clock::now() - start).count() >= Approx(sleepTime).epsilon(0.1));
    }

    SECTION("millisecond sleep") {
        TestType sleepTime = GENERATE(take(5, random(TestType(10.), TestType(200.))));

        auto start = clock::now();
        Utility::sleepMilliseconds(sleepTime);
        REQUIRE(duration_cast<milliseconds>(clock::now() - start).count() >= Approx(sleepTime).epsilon(0.05));
    }

    SECTION("second sleep") {
        TestType sleepTime = GENERATE(take(5, random(TestType(1.), TestType(2.))));

        auto start = clock::now();
        Utility::sleepSeconds(sleepTime);
        REQUIRE(duration_cast<milliseconds>(clock::now() - start).count() / 1000. == Approx(sleepTime).epsilon(0.05));
    }
}

SCENARIO("commandline", "[utility][commandline]") {
    const std::string testFile = "testfile.txt";
    const std::string fileContent = "this is a test string\n";

    REQUIRE(Utility::fileExists(testFile) == false);
    REQUIRE(Utility::readFile(testFile) == "");

    REQUIRE(Utility::getSystemCallOutput("touch " + testFile) == "");

    REQUIRE(Utility::fileExists(testFile) == true);
    REQUIRE(Utility::readFile(testFile) == "");

    REQUIRE(Utility::getSystemCallOutput("echo \"" + fileContent + "\" > " + testFile) == "");

    REQUIRE(Utility::readFile(testFile) == fileContent);

    REQUIRE(Utility::getSystemCallOutput("cat " + testFile) == fileContent);

    REQUIRE(Utility::getSystemCallOutput("rm " + testFile) == "");

    REQUIRE(Utility::fileExists(testFile) == false);

    REQUIRE(Utility::getSystemCallOutput("echo \"teststring for testing\"") == "teststring for testing");
    REQUIRE(Utility::getSystemCallOutput("echo \"this output is two\nlines long\"") == "this output is two\nlines long");
}
