#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <pizzy/common/Exception.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/VirtualPinProvider.h>

#include <chrono>
#include <thread>

using namespace pizzy::core;

SCENARIO("VirtualPinProvider", "[pinprovider]") {
    auto& board = VirtualPinProvider::getInstance();

    REQUIRE(board.getName() == "Virtual Pin Provider");
    REQUIRE(board.getShortName() == "VIRT");

    WHEN("pin number not registered") {
        int pinNumber = GENERATE(take(5, random(1, 20)));

        THEN("exception is thrown on getPinState") {
            REQUIRE_THROWS_AS(board.getPinState(pinNumber), pizzy::common::Exception);
        }
    }

    SECTION("Request pins") {
        const int pinNumber1 = GENERATE(take(5, random(1, 20)));
        const int pinNumber2 = GENERATE(take(1, random(21, 40)));

        std::unique_ptr<Pin> pin1, pin2;
        REQUIRE_NOTHROW(pin1 = board.getPin(pinNumber1));
        REQUIRE_NOTHROW(pin2 = board.getPin(pinNumber2));

        REQUIRE(board.getPinState(pinNumber1).pinMode == VirtualPinState::Mode::OUT);
        REQUIRE(board.getPinState(pinNumber2).pinMode == VirtualPinState::Mode::OUT);
        REQUIRE(board.getPinState(pinNumber1).currentState == false);
        REQUIRE(board.getPinState(pinNumber2).currentState == false);

        REQUIRE(pin1->getPinNumber() == pinNumber1);
        REQUIRE(pin2->getPinNumber() == pinNumber2);
        REQUIRE(pin1->read() == false);
        REQUIRE(pin2->read() == false);

        pin1->write(true);
        REQUIRE(board.getPinState(pinNumber1).currentState == true);
        REQUIRE(board.getPinState(pinNumber2).currentState == false);
        REQUIRE(pin1->read() == true);
        REQUIRE(pin2->read() == false);

        pin2->write(true);
        REQUIRE(board.getPinState(pinNumber1).currentState == true);
        REQUIRE(board.getPinState(pinNumber2).currentState == true);
        REQUIRE(pin1->read() == true);
        REQUIRE(pin2->read() == true);

        pin1->write(false);
        REQUIRE(board.getPinState(pinNumber1).currentState == false);
        REQUIRE(board.getPinState(pinNumber2).currentState == true);
        REQUIRE(pin1->read() == false);
        REQUIRE(pin2->read() == true);

        pin2->write(false);
        REQUIRE(board.getPinState(pinNumber1).currentState == false);
        REQUIRE(board.getPinState(pinNumber2).currentState == false);
        REQUIRE(pin1->read() == false);
        REQUIRE(pin2->read() == false);
    }

    SECTION("colliding pins") {
        const int pinNumber1 = GENERATE(take(5, random(1, 20)));

        std::unique_ptr<Pin> pin1;
        REQUIRE_NOTHROW(pin1 = board.getPin(pinNumber1));

        REQUIRE_THROWS_AS(board.getPin(pinNumber1), pizzy::common::Exception);
    }

    SECTION("bulk writing") {
        const int pinNumber1 = GENERATE(take(5, random(1, 20)));
        const int pinNumber2 = GENERATE(take(1, random(21, 40)));

        std::unique_ptr<Pin> pin1, pin2;
        REQUIRE_NOTHROW(pin1 = board.getPin(pinNumber1));
        REQUIRE_NOTHROW(pin2 = board.getPin(pinNumber2));

        WHEN("Bulk Writing pins") {
            pin1->bulkWrite(true);
            pin2->bulkWrite(true);

            THEN("Changes are not applied immediately") {
                REQUIRE(board.getPinState(pinNumber1).currentState == false);
                REQUIRE(board.getPinState(pinNumber2).currentState == false);
            }

            Pin::bulkFlush();

            THEN("Changes are applied after flush") {
                REQUIRE(board.getPinState(pinNumber1).currentState == true);
                REQUIRE(board.getPinState(pinNumber2).currentState == true);
            }
        }

        WHEN("Setting the same pin multiple times") {
            bool state = GENERATE(true, false);

            pin1->bulkWrite(state);

            THEN("Changes are not applied immediately") {
                REQUIRE(board.getPinState(pinNumber1).currentState == false);
            }

            pin1->bulkWrite(!state);

            THEN("Changes are not applied immediately") {
                REQUIRE(board.getPinState(pinNumber1).currentState == false);
            }

            Pin::bulkFlush();

            THEN("Last set value counts") {
                REQUIRE(board.getPinState(pinNumber1).currentState == !state);
            }
        }
    }
}
