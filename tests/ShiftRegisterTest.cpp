#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include <pizzy/common/Exception.h>
#include <pizzy/core/VirtualPinProvider.h>
#include <pizzy/peripherals/LED.h>
#include <pizzy/peripherals/ShiftRegister.h>

#include <chrono>
#include <string>
#include <thread>

using namespace pizzy::common;
using namespace pizzy::core;
using namespace pizzy::peripherals;

SCENARIO("constructing a shift register on a virtual board", "[shiftregister][peripheral]") {
    auto& board = VirtualPinProvider::getInstance();

    auto dataPin = GENERATE(take(2, random(1, 20)));
    auto clockPin = GENERATE_REF(take(2, filter(
                                             [&dataPin](int n) {
                                                 return n != dataPin;
                                             },
                                             random(1, 20))));
    auto latchPin = GENERATE_REF(take(2, filter(
                                             [&dataPin, &clockPin](int n) {
                                                 return n != dataPin && n != clockPin;
                                             },
                                             random(1, 20))));
    auto enablePin = GENERATE_REF(take(2, filter(
                                              [&dataPin, &clockPin, &latchPin](int n) {
                                                  return n != dataPin && n != clockPin && n != latchPin;
                                              },
                                              random(1, 20))));
    auto clearPin = GENERATE_REF(take(2, filter(
                                             [&dataPin, &clockPin, &latchPin, &enablePin](int n) {
                                                 return n != dataPin && n != clockPin && n != latchPin && n != enablePin;
                                             },
                                             random(1, 20))));

    bool useEnablePin = enablePin < 10;
    bool useClearPin = clearPin < 10;

    std::uint32_t size = GENERATE(take(3, random(8, 32)));

    const std::string expectedName = "Shift Register";

    GIVEN("invalid constructor arguments") {
        WHEN("data pin is nullptr") {
            THEN("exception is thrown") {
                REQUIRE_THROWS(ShiftRegister({.data = nullptr,
                                              .clock = board.getPin(clockPin),
                                              .latch = board.getPin(latchPin),
                                              .clear = useClearPin ? board.getPin(clearPin) : nullptr,
                                              .enable = useEnablePin ? board.getPin(enablePin) : nullptr,
                                              .size = size}));
            }
        }
        WHEN("clock pin is nullptr") {
            THEN("exception is thrown") {
                REQUIRE_THROWS(ShiftRegister({.data = board.getPin(dataPin),
                                              .clock = nullptr,
                                              .latch = board.getPin(latchPin),
                                              .clear = useClearPin ? board.getPin(clearPin) : nullptr,
                                              .enable = useEnablePin ? board.getPin(enablePin) : nullptr,
                                              .size = size}));
            }
        }
        WHEN("latch pin is nullptr") {
            THEN("exception is thrown") {
                REQUIRE_THROWS(ShiftRegister({.data = board.getPin(dataPin),
                                              .clock = board.getPin(clockPin),
                                              .latch = nullptr,
                                              .clear = useClearPin ? board.getPin(clearPin) : nullptr,
                                              .enable = useEnablePin ? board.getPin(enablePin) : nullptr,
                                              .size = size}));
            }
        }
        WHEN("size is zero") {
            THEN("exception is thrown") {
                REQUIRE_THROWS(ShiftRegister({.data = board.getPin(dataPin),
                                              .clock = board.getPin(clockPin),
                                              .latch = board.getPin(latchPin),
                                              .clear = useClearPin ? board.getPin(clearPin) : nullptr,
                                              .enable = useEnablePin ? board.getPin(enablePin) : nullptr,
                                              .size = 0}));
            }
        }
    }

    GIVEN("a default shift register") {
        ShiftRegister testShiftRegister({.data = board.getPin(dataPin),
                                         .clock = board.getPin(clockPin),
                                         .latch = board.getPin(latchPin),
                                         .clear = useClearPin ? board.getPin(clearPin) : nullptr,
                                         .enable = useEnablePin ? board.getPin(enablePin) : nullptr,
                                         .size = size});

        THEN("has correct values set") {
            REQUIRE(board.getPinState(dataPin).pinMode == VirtualPinState::Mode::OUT);
            REQUIRE(board.getPinState(clockPin).pinMode == VirtualPinState::Mode::OUT);
            REQUIRE(board.getPinState(latchPin).pinMode == VirtualPinState::Mode::OUT);
            if (useEnablePin) {
                REQUIRE(board.getPinState(enablePin).pinMode == VirtualPinState::Mode::OUT);
                REQUIRE(board.getPinState(enablePin).currentState == false);
            }
            if (useClearPin) {
                REQUIRE(board.getPinState(clearPin).pinMode == VirtualPinState::Mode::OUT);
                REQUIRE(board.getPinState(clearPin).currentState == true);
            }

            REQUIRE(board.getPinState(dataPin).currentState == false);
            REQUIRE(board.getPinState(clockPin).currentState == false);
            REQUIRE(board.getPinState(latchPin).currentState == false);

            REQUIRE(testShiftRegister.getName() == expectedName);
        }

        WHEN("getting a pin from the shift register") {

            auto outPinNumber = GENERATE(range(0, 7));
            auto pin = testShiftRegister.getPin(outPinNumber);

            THEN("it's usable") {
                REQUIRE_NOTHROW(pin->write(true));
                REQUIRE_THROWS(pin->read());

                auto led = LED({.pin = std::move(pin)});

                REQUIRE_NOTHROW(led.setState(true));
                REQUIRE(led.getState() == true);

                REQUIRE(board.getPinState(dataPin).currentState == false);
                REQUIRE(board.getPinState(clockPin).currentState == false);
                REQUIRE(board.getPinState(latchPin).currentState == false);

                if (useEnablePin) {
                    REQUIRE(board.getPinState(enablePin).currentState == false);
                }
                if (useClearPin) {
                    REQUIRE(board.getPinState(clearPin).currentState == true);
                }
            }
        }
    }
}
