#include <pizzy/common/Exception.h>
#include <pizzy/core/SPI.h>
#include <pizzy/core/SPIProvider.h>

#include <memory>
#include <sstream>
#include <string>

namespace pizzy::core {

    std::ostream& operator<<(std::ostream& os, const SPIDataMode& mode) {
        switch (mode) {
        case SPIDataMode::MODE0:
            os << "SPI Data Mode 0 (CPOL = 0, CPHA = 0)";
            break;
        case SPIDataMode::MODE1:
            os << "SPI Data Mode 1 (CPOL = 0, CPHA = 1)";
            break;
        case SPIDataMode::MODE2:
            os << "SPI Data Mode 2 (CPOL = 1, CPHA = 1)";
            break;
        case SPIDataMode::MODE3:
            os << "SPI Data Mode 3 (CPOL = 1, CPHA = 0)";
            break;
        default:
            os << "Unknown SPI Data Mode";
            break;
        }
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const BitOrder& order) {
        switch (order) {
        case BitOrder::MOST_SIGNIFICANT_BIT_FIRST:
            os << "Most Significant Bit First";
            break;
        case BitOrder::LEAST_SIGNIFICANT_BIT_FIRST:
            os << "Least Significant Bit First";
            break;
        default:
            os << "Unknown Bit Order";
            break;
        }
        return os;
    }

    std::unique_ptr<SPI> SPIProvider::createSPI(const int index) {
        if (spis.find(index) != spis.end()) {
            // SPI already registered
            std::ostringstream ss;
            ss << "Cannot create SPI, SPI with index " << index << " is already registered!";
            throw pizzy::common::Exception(ss.str());
        }

        spis.emplace(index);
        return std::unique_ptr<SPI>(new SPI(index, *this));
    }

    void SPIProvider::destroySPI(const int index) {
        if (spis.find(index) == spis.end()) {
            // trying to destroy a SPI that shouldn't exist
            std::ostringstream ss;
            ss << "SPI" << index << " is being destroyed, but was never created!";
            throw pizzy::common::Exception(ss.str());
        }

        spis.erase(index);

        deleteSPI(index);
    }

    void SPIProvider::setDataMode(const int index, const bool polarity, const bool phase) {
        // the order is intentional
        if (!polarity) {
            if (!phase) {
                setDataMode(index, SPIDataMode::MODE0);
            } else {
                setDataMode(index, SPIDataMode::MODE1);
            }
        } else {
            if (phase) {
                setDataMode(index, SPIDataMode::MODE2);
            } else {
                setDataMode(index, SPIDataMode::MODE3);
            }
        }
    }

} // namespace pizzy::core
