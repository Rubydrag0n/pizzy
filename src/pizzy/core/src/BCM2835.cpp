#include <pizzy/common/Exception.h>
#include <pizzy/common/definitions.h>
#include <pizzy/core/BCM2835.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/SPI.h>
#include <pizzy/core/SPIProvider.h>

#include <bcm2835.h>

#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <vector>

enum { SPI0 = 0, SPI1 = 1, SPICOUNT };

std::map<int, std::vector<int>> SPI_RESERVED_PINS = {
    // SPI0 reserved pins:
    {SPI0, {7, 8, 9, 10, 11}},
    // SPI1 reserved pins:
    {SPI1, {16, 17, 18, 19, 20, 21}}};

namespace {

    using namespace pizzy::core;

    /**
     * @brief Gets all or none of the given list of pins and returns them
     */
    std::vector<std::unique_ptr<Pin>> getPins(const std::vector<int>& pins) {
        std::vector<std::unique_ptr<Pin>> reservedPins;
        for (const auto& pin : pins) {
            try {
                reservedPins.emplace_back(BCM2835::getInstance().getPin(pin));
            } catch (const pizzy::common::Exception& e) {
                std::ostringstream os;
                os << "Failed to reserve all required Pins for SPI on bcm2835\n";
                os << "Required Pins are: ";
                bool first = true;
                for (const auto& p : pins) {
                    if (first) {
                        first = false;
                    } else {
                        os << ", ";
                    }
                    os << p;
                    if (p == pin) {
                        os << " (failed to reserve)";
                    }
                }
                throw pizzy::common::Exception(os.str());
            }
        }
        return reservedPins;
    }

    int bitOrderToBCM(const BitOrder& order) {
        switch (order) {
        case BitOrder::MOST_SIGNIFICANT_BIT_FIRST:
            return BCM2835_SPI_BIT_ORDER_MSBFIRST;
        case BitOrder::LEAST_SIGNIFICANT_BIT_FIRST:
            return BCM2835_SPI_BIT_ORDER_LSBFIRST;
        default:
            throw pizzy::common::Exception("Invalid bit order given");
        }
    }

    int dataModeToBCM(const SPIDataMode& mode) {
        switch (mode) {
        case SPIDataMode::MODE0:
            return BCM2835_SPI_MODE0;
        case SPIDataMode::MODE1:
            return BCM2835_SPI_MODE1;
        case SPIDataMode::MODE2:
            return BCM2835_SPI_MODE3;
        case SPIDataMode::MODE3:
            return BCM2835_SPI_MODE2;
        default:
            throw pizzy::common::Exception("Invalid SPI data mode given");
        }
    }

} // namespace

namespace pizzy::core {

    BCM2835::BCM2835()
        : Provider("BCM2835", "BCM") {
        if (!bcm2835_init()) {
            throw common::Exception("Failed to initialize BCM2835!");
        }
    }

    BCM2835::~BCM2835() {
        if (!bcm2835_close()) {
            std::cerr << "Failed to close BCM2835" << std::endl;
        }
    }

    BCM2835& BCM2835::getInstance() {
        static BCM2835 instance;
        return instance;
    }

#pragma region PinProvider
    std::unique_ptr<Pin> BCM2835::getPin(const int pinNumber) {
        auto pin = PinProvider::createPin(pinNumber);

        // set pin to output
        bcm2835_gpio_fsel(pinNumber, BCM2835_GPIO_FSEL_OUTP);
        return pin;
    }

    void BCM2835::writePin(const int pinNumber, const bool state) {
        bcm2835_gpio_write(pinNumber, state ? HIGH : LOW);
    }

    void BCM2835::bulkFlush(const std::vector<std::pair<int, bool>>& ops) {
        for (const auto& op : ops) {
            writePin(op.first, op.second);
        }
    }

    bool BCM2835::readPin(const int /*pinNumber*/) {
        throw common::Exception("BCM2835::readPin is not implemented");
    }

    void BCM2835::deletePin(const int /*pinNumber*/) {
    }
#pragma endregion PinProvider

#pragma region SPIProvider
    std::unique_ptr<SPI> BCM2835::getSPI(const int index) {
        if (index >= SPICOUNT) {
            std::ostringstream os;
            os << "Failed to get SPI with invalid index " << index;
            throw pizzy::common::Exception(os.str());
        }

        // try to reserve the pins needed, this call will throw if impossible
        auto reservedPins = getPins(SPI_RESERVED_PINS[index]);
        // keep them in a local container so they will be destroyed if any other call fails

        if (index == SPI0) {
            if (!bcm2835_spi_begin()) {
                throw pizzy::common::Exception("Failed to start SPI, are you running as root?");
            }
        } else if (index == SPI1) {
            if (!bcm2835_aux_spi_begin()) {
                throw pizzy::common::Exception("Failed to start SPI, are you running as root?");
            }
        }

        // now put the reserved pins into the storage until the spi is destroyed
        spiReservedPins[index] = std::move(reservedPins);

        // if everything was successfull -> create the SPI
        return SPIProvider::createSPI(index);
    }

    void BCM2835::deleteSPI(const int index) {
        switch (index) {
        case SPI0:
            bcm2835_spi_end();
            break;
        case SPI1:
            bcm2835_aux_spi_end();
            break;
        default:
            throw pizzy::common::Exception("Failed to delete SPI, invalid index" + std::to_string(index));
        }

        // delete the reserved pins
        spiReservedPins[index].clear();
    }

    void BCM2835::selectSlave(const int index, const int slave) {
        switch (index) {
        case SPI0:
            bcm2835_spi_chipSelect(slave);
            break;
        case SPI1:
            throw pizzy::common::Exception("Slave select is not available on SPI1");
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    void BCM2835::setFrequency(const int index, const int frequency) {
        switch (index) {
        case SPI0:
            bcm2835_spi_set_speed_hz(frequency);
            break;
        case SPI1:
            /** TODO: implement this to the best of knowledge */
            throw pizzy::common::Exception("Setting frequency is currently unimplemented for SPI1");
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    void BCM2835::setBitOrder(const int index, const BitOrder order) {
        switch (index) {
        case SPI0:
            bcm2835_spi_setBitOrder(bitOrderToBCM(order));
            break;
        case SPI1:
            throw pizzy::common::Exception("SPI1 does not support setting bit order");
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    void BCM2835::setSlaveSelectPolarity(const int index, const int slave, const bool active) {
        switch (index) {
        case SPI0:
            bcm2835_spi_setChipSelectPolarity(slave, active);
            break;
        case SPI1:
            throw pizzy::common::Exception("SPI1 does not support chip select");
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    void BCM2835::setDataMode(const int index, const SPIDataMode mode) {
        switch (index) {
        case SPI0:
            bcm2835_spi_setDataMode(dataModeToBCM(mode));
            break;
        case SPI1:
            throw pizzy::common::Exception("SPI1 does not support changing SPI data mode");
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    void BCM2835::send(const int index, const std::vector<std::uint8_t>& data, const int slave) {
        send(index, data.data(), data.size(), slave);
    }

    void BCM2835::send(const int index, const std::uint8_t* data, const int length, const int slave) {
        // slave needs to be selected first, even if no data is sent
        if (slave != NO_SLAVE_CHANGE) {
            selectSlave(index, slave);
        }
        if (length <= 0) {
            return;
        }

        switch (index) {
        case SPI0:
            bcm2835_spi_writenb(reinterpret_cast<const char*>(data), length);
            break;
        case SPI1:
            bcm2835_aux_spi_writenb(reinterpret_cast<const char*>(data), length);
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }
    }

    std::vector<std::uint8_t> BCM2835::receive(const int index, const int length, const int slave) {
        // receive is just transfer with sending null bytes
        std::vector<std::uint8_t> send(length, 0);

        return transfer(index, send, slave);
    }

    std::vector<std::uint8_t> BCM2835::transfer(const int index, const std::vector<std::uint8_t>& data, const int slave) {
        // slave needs to be selected first, even if no data is sent
        if (slave != NO_SLAVE_CHANGE) {
            selectSlave(index, slave);
        }
        if (data.size() == 0) {
            return {};
        }

        std::vector<std::uint8_t> result(data.size());
        result.resize(data.size());

        char* inputData = reinterpret_cast<char*>(const_cast<std::uint8_t*>(data.data()));
        char* resultData = reinterpret_cast<char*>(result.data());

        switch (index) {
        case SPI0:
            // evil const_cast, but transfernb won't touch the data to send (presumably this is just a mistake in the function definition)
            bcm2835_spi_transfernb(inputData, resultData, data.size());
            break;
        case SPI1:
            bcm2835_aux_spi_transfernb(inputData, resultData, data.size());
            break;
        default:
            throw pizzy::common::Exception("Failed to access SPI, invalid index" + std::to_string(index));
        }

        return result;
    }
#pragma endregion SPIProvider

} // namespace pizzy::core
