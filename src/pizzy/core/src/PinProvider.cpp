#include <pizzy/common/Exception.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/PinProvider.h>

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

namespace pizzy::core {

    std::vector<PinProvider*> PinProvider::pinProviderRegistry;


    PinProvider::PinProvider() {
        pinProviderRegistry.push_back(this);
    }

    PinProvider::~PinProvider() {
        pinProviderRegistry.erase(std::remove(pinProviderRegistry.begin(), pinProviderRegistry.end(), this), pinProviderRegistry.end());
    }

    std::unique_ptr<Pin> PinProvider::createPin(const int pinNumber) {
        if (pins.find(pinNumber) != pins.end()) {
            // pin already registered
            std::ostringstream ss;
            ss << "Cannot create pin, Pin " << pinNumber << " is already registered!";
            throw pizzy::common::Exception(ss.str());
        }

        pins.emplace(pinNumber);
        return std::unique_ptr<Pin>(new Pin(pinNumber, *this));
    }

    void PinProvider::destroyPin(const int pinNumber) {
        if (pins.find(pinNumber) == pins.end()) {
            // trying to destroy a pin that shouldn't exist
            std::ostringstream ss;
            ss << "Pin " << pinNumber << " is being destroyed, but was never created!";
            throw pizzy::common::Exception(ss.str());
        }

        pins.erase(pinNumber);

        deletePin(pinNumber);
    }

    void PinProvider::bulkWrite(const int pinNumber, const bool state) {
        bulkOperations.emplace_back(std::pair<int, bool>{pinNumber, state});
    }

    void PinProvider::bulkFlushAll() {
        for (auto& provider : pinProviderRegistry) {
            provider->bulkFlush(provider->getBulkOperations());
            provider->clearBulkOperations();
        }
    }

    const std::vector<std::pair<int, bool>>& PinProvider::getBulkOperations() const {
        return bulkOperations;
    }

    void PinProvider::clearBulkOperations() {
        bulkOperations.clear();
    }

} // namespace pizzy::core
