#include <pizzy/core/SPI.h>
#include <pizzy/core/SPIProvider.h>

#include <sstream>

namespace pizzy::core {

    SPI::SPI(const int index, SPIProvider& provider)
        : index(index)
        , spiProvider(provider) {
    }

    SPI::~SPI() {
        spiProvider.destroySPI(index);
    }

    int SPI::getIndex() const {
        return index;
    }

    std::string SPI::getName() const {
        std::ostringstream os;
        os << spiProvider.getShortName() << "SPI" << index;
        return os.str();
    }

    std::ostream& operator<<(std::ostream& os, const SPI& spi) {
        os << spi.getName();
        return os;
    }

    void SPI::selectSlave(const int slave) {
        spiProvider.selectSlave(index, slave);
    }

    void SPI::setFrequency(const int frequency) {
        spiProvider.setFrequency(index, frequency);
    }

    void SPI::setBitOrder(const BitOrder order) {
        spiProvider.setBitOrder(index, order);
    }

    void SPI::setSlaveSelectPolarity(const int slave, const bool active) {
        spiProvider.setSlaveSelectPolarity(index, slave, active);
    }

    void SPI::setDataMode(const bool polarity, const bool phase) {
        spiProvider.setDataMode(index, polarity, phase);
    }

    void SPI::setDataMode(const SPIDataMode mode) {
        spiProvider.setDataMode(index, mode);
    }

    void SPI::send(const std::vector<std::uint8_t>& data, const int slave) {
        spiProvider.send(index, data, slave);
    }

    void SPI::send(const std::uint8_t* data, const int length, const int slave) {
        spiProvider.send(index, data, length, slave);
    }

    std::vector<std::uint8_t> SPI::receive(const int length, const int slave) {
        return spiProvider.receive(index, length, slave);
    }

    std::vector<std::uint8_t> SPI::transfer(const std::vector<std::uint8_t>& data, const int slave) {
        return spiProvider.transfer(index, data, slave);
    }

} // namespace pizzy::core
