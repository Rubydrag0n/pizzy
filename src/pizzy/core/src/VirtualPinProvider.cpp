#include <pizzy/common/Exception.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/SPI.h>
#include <pizzy/core/SPIProvider.h>
#include <pizzy/core/VirtualPinProvider.h>

#include <iostream>

namespace pizzy::core {

    VirtualPinProvider::VirtualPinProvider()
        : Provider("Virtual Pin Provider", "VIRT") {
    }

    VirtualPinProvider& VirtualPinProvider::getInstance() {
        static VirtualPinProvider instance;
        return instance;
    }

#pragma endregion PinProvider

    void VirtualPinProvider::checkPinExists(const int pinNumber, const char* caller) const {
        if (pinStates.find(pinNumber) == pinStates.end()) {
            throw pizzy::common::Exception("Trying to access nonexistent pin " + std::to_string(pinNumber) + " in \"" + caller + "\"");
        }
    }

    std::unique_ptr<Pin> VirtualPinProvider::getPin(const int pinNumber) {
        if (pinStates.find(pinNumber) != pinStates.end()) {
            throw pizzy::common::Exception("Trying to create pin " + std::to_string(pinNumber) + " but it already existed");
        }
        auto pin = createPin(pinNumber);

        pinStates[pinNumber] = {.pinMode = VirtualPinState::Mode::OUT, .currentState = false};

        return pin;
    }

    VirtualPinState& VirtualPinProvider::getPinState(const int pinNumber) {
        checkPinExists(pinNumber, "getPinState");
        return pinStates[pinNumber];
    }

    void VirtualPinProvider::writePin(const int pinNumber, const bool state) {
        checkPinExists(pinNumber, "writePin");
        auto& pinState = pinStates[pinNumber];
        if (pinState.pinMode != VirtualPinState::Mode::OUT) {
            throw pizzy::common::Exception("Trying to write to pin " + std::to_string(pinNumber) + " but it is not set to output mode");
        }
        pinState.currentState = state;
    }

    void VirtualPinProvider::bulkFlush(const std::vector<std::pair<int, bool>>& ops) {
        for (const auto& op : ops) {
            writePin(op.first, op.second);
        }
    }

    bool VirtualPinProvider::readPin(const int pinNumber) {
        checkPinExists(pinNumber, "readPin");
        const auto& pinState = pinStates[pinNumber];
        return pinState.currentState;
    }

    void VirtualPinProvider::deletePin(const int pinNumber) {
        checkPinExists(pinNumber, "deletePin");
        pinStates.erase(pinNumber);
    }
#pragma endregion PinProvider

#pragma region SPIProvider

    void VirtualPinProvider::checkSPIExists(const int index, const char* caller) const {
        if (spiStates.find(index) == spiStates.end()) {
            throw pizzy::common::Exception("Trying to access nonexistent spi " + std::to_string(index) + " in \"" + caller + "\"");
        }
    }

    void VirtualPinProvider::checkSlave(const int slave, const char* caller) const {
        if (slave < 0 && slave != NO_SLAVE_CHANGE) {
            throw pizzy::common::Exception("Invalid slave " + std::to_string(slave) + " was given in \"" + caller + "\"");
        }
    }

    std::unique_ptr<SPI> VirtualPinProvider::getSPI(const int index) {
        if (spiStates.find(index) != spiStates.end()) {
            throw pizzy::common::Exception("Trying to create spi " + std::to_string(index) + " which already exists");
        }
        spiStates.insert({index, {}});
        return SPIProvider::createSPI(index);
    }

    VirtualSPIState& VirtualPinProvider::getSPIState(const int index) {
        checkSPIExists(index, "getSPIState");
        return spiStates[index];
    }

    void VirtualPinProvider::deleteSPI(const int index) {
        checkSPIExists(index, "deleteSPI");
        spiStates.erase(index);
    }

    void VirtualPinProvider::selectSlave(const int index, const int slave) {
        checkSPIExists(index, "selectSlave");
        checkSlave(slave, "selectSlave");
        std::cout << "Selecting slave " << slave << " at index " << index << std::endl;
        spiStates[index].selectedSlave = slave;
    }

    void VirtualPinProvider::setFrequency(const int index, const int frequency) {
        checkSPIExists(index, "setFrequency");
        std::cout << "Setting frequency " << frequency << " at index " << index << std::endl;
        spiStates[index].frequency = frequency;
    }

    void VirtualPinProvider::setBitOrder(const int index, const BitOrder order) {
        checkSPIExists(index, "setBitOrder");
        std::cout << "Setting bit order " << order << " at index " << index << std::endl;
        spiStates[index].bitOrder = order;
    }

    void VirtualPinProvider::setSlaveSelectPolarity(const int index, const int slave, const bool active) {
        checkSPIExists(index, "setSlaveSelectPolarity");
        checkSlave(slave, "setSlaveSelectPolarity");
        std::cout << "Setting slave select polarity " << (active ? "active" : "inactive") << " for slave " << slave << " at index " << index
                  << std::endl;
        spiStates[index].slavePolarity = active;
    }

    void VirtualPinProvider::setDataMode(const int index, const SPIDataMode mode) {
        checkSPIExists(index, "setDataMode");
        std::cout << "Setting data mode to " << mode << " at index " << index << std::endl;
        spiStates[index].mode = mode;
    }

    void VirtualPinProvider::send(const int index, const std::vector<std::uint8_t>& data, const int slave) {
        checkSPIExists(index, "send (vector)");
        checkSlave(slave, "send (vector)");
        if (slave != NO_SLAVE_CHANGE) {
            spiStates[index].selectedSlave = slave;
        }
        std::cout << "Sending " << data.size() << " Bytes of data to slave " << spiStates[index].selectedSlave << " at index " << index << std::endl;
        spiStates[index].totalBytesSent += data.size();
    }

    void VirtualPinProvider::send(const int index, const std::uint8_t* /*data*/, const int length, const int slave) {
        checkSPIExists(index, "send (pointer)");
        checkSlave(slave, "send (pointer)");
        if (length < 0) {
            throw pizzy::common::Exception("Trying to send negative number of bytes");
        }
        if (slave != NO_SLAVE_CHANGE) {
            spiStates[index].selectedSlave = slave;
        }
        std::cout << "Sending " << length << " Bytes of data to slave " << spiStates[index].selectedSlave << " at index " << index << std::endl;
        spiStates[index].totalBytesSent += length;
    }

    std::vector<std::uint8_t> VirtualPinProvider::receive(const int index, const int length, const int slave) {
        checkSPIExists(index, "receive");
        checkSlave(slave, "receive");
        if (length < 0) {
            throw pizzy::common::Exception("Trying to receive negative number of bytes");
        }
        if (slave != NO_SLAVE_CHANGE) {
            spiStates[index].selectedSlave = slave;
        }
        std::cout << "Receiving " << length << " Bytes of data from slave " << spiStates[index].selectedSlave << " at index " << index << std::endl;
        spiStates[index].totalBytesReceived += length;

        std::vector<std::uint8_t> res;
        res.resize(length, 0);
        return res;
    }

    std::vector<std::uint8_t> VirtualPinProvider::transfer(const int index, const std::vector<std::uint8_t>& data, const int slave) {
        checkSPIExists(index, "transfer");
        checkSlave(slave, "transfer");
        if (slave != NO_SLAVE_CHANGE) {
            spiStates[index].selectedSlave = slave;
        }
        std::cout << "Transfering " << data.size() << " Bytes of data to and from slave " << spiStates[index].selectedSlave << " at index " << index
                  << std::endl;
        spiStates[index].totalBytesReceived += data.size();
        spiStates[index].totalBytesSent += data.size();

        std::vector<std::uint8_t> res;
        res.resize(data.size(), 0);
        return res;
    }
#pragma endregion SPIProvider

} // namespace pizzy::core
