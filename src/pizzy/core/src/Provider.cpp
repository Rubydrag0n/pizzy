#include <pizzy/core/Provider.h>

namespace pizzy::core {

    Provider::Provider(std::string name, std::string shortName)
        : name(std::move(name))
        , shortName(std::move(shortName)) {
    }

    std::string Provider::getName() const {
        return name;
    }

    std::string Provider::getShortName() const {
        return shortName;
    }

} // namespace pizzy::core
