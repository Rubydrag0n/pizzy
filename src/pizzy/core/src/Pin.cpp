#include <pizzy/common/Utility.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/PinProvider.h>

#include <algorithm>
#include <ostream>
#include <string>
#include <vector>

namespace pizzy::core {

    Pin::Pin(const int pin, pizzy::core::PinProvider& provider)
        : pin(pin)
        , pinProvider(provider) {
    }

    Pin::~Pin() {
        pinProvider.destroyPin(pin);
    }

    int Pin::getPinNumber() const {
        return pin;
    }

    std::string Pin::getName() const {
        std::stringstream ss;
        ss << pinProvider.getShortName() << "PIN" << pin;
        return ss.str();
    }

    std::ostream& operator<<(std::ostream& os, const Pin& pin) {
        os << pin.getName();
        return os;
    }

    void Pin::write(const bool state) const {
        pinProvider.writePin(pin, state);
    }

    void Pin::bulkWrite(const bool state) const {
        pinProvider.bulkWrite(pin, state);
    }

    void Pin::bulkFlush() {
        PinProvider::bulkFlushAll();
    }

    bool Pin::read() const {
        return pinProvider.readPin(pin);
    }

} // namespace pizzy::core
