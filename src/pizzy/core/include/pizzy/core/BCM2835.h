/** @file */
#pragma once

#include <pizzy/core/PinProvider.h>
#include <pizzy/core/SPIProvider.h>

#include <map>
#include <memory>
#include <vector>

namespace pizzy::core {

    /**
     * @brief The standard Raspberry PI board BCM2835
     */
    class BCM2835 : public PinProvider, public SPIProvider {
    public:
        ~BCM2835() override;

        /**
         * @brief Singleton implementation, use this static function to get the object
         */
        [[nodiscard]] static BCM2835& getInstance();

        [[nodiscard]] std::unique_ptr<Pin> getPin(int pinNumber) final;

        [[nodiscard]] std::unique_ptr<SPI> getSPI(int index) final;

    protected:
#pragma region PinProvider
        void writePin(int pinNumber, bool state) final;

        void bulkFlush(const std::vector<std::pair<int, bool>>& ops) final;

        [[nodiscard]] bool readPin(int pinNumber) final;

        void deletePin(int pinNumber) final;
#pragma endregion PinProvider

#pragma region SPIProvider
        void deleteSPI(int index) final;

        void selectSlave(int index, int slave) final;

        void setFrequency(int index, int frequency) final;

        void setBitOrder(int index, BitOrder order) final;

        void setSlaveSelectPolarity(int index, int slave, bool active) final;

        void setDataMode(int index, SPIDataMode mode) final;

        void send(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) final;

        void send(int index, const std::uint8_t* data, int length, int slave = NO_SLAVE_CHANGE) final;

        [[nodiscard]] std::vector<std::uint8_t> receive(int index, int length, int slave = NO_SLAVE_CHANGE) final;

        [[nodiscard]] std::vector<std::uint8_t> transfer(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) final;
#pragma endregion SPIProvider

    private:
        BCM2835();

        /**
         * @brief A map keeping reserved Pins alive
         * @details The key is the SPI that reserves the pins
         */
        std::map<int, std::vector<std::unique_ptr<Pin>>> spiReservedPins;
    };

} // namespace pizzy::core
