/** @file */
#pragma once

#include <pizzy/core/PinProvider.h>
#include <pizzy/core/SPIProvider.h>

#include <map>
#include <memory>

namespace pizzy::core {

    /**
     * @brief The state of a virtual pin, i.e. its mode and current state
     */
    struct VirtualPinState {
        /**
         * @brief The mode of a pin, IN (the pin reads) or OUT (the pin writes)
         */
        enum class Mode { IN, OUT };

        /**
         * @brief The mode of the pin, IN (the pin reads) or OUT (the pin writes)
         */
        Mode pinMode;

        /**
         * @brief In mode IN this means the "input" is on/off, in mode OUT
         * this means the Pin is turned on/off
         */
        bool currentState = false;
    };

    /**
     * @brief The state of a virtual spi
     */
    struct VirtualSPIState {

        /**
         * @brief Count of every byte that was given by the user to send
         * @details Includes bytes from send and transfer calls, but not receive
         */
        unsigned totalBytesSent = 0;

        /**
         * @brief Count of every byte that was requested by the user to be received
         * @details Includes bytes from transfer and receive calls, but not send
         */
        unsigned totalBytesReceived = 0;

        /**
         * @brief The currently selected slave
         */
        int selectedSlave = 0;

        /**
         * @brief The currently set frequency
         */
        int frequency = 0;

        /**
         * @brief The currently set bit order
         */
        BitOrder bitOrder = BitOrder::MOST_SIGNIFICANT_BIT_FIRST;

        /**
         * @brief The current slave polarity
         */
        bool slavePolarity = false;

        /**
         * @brief The current SPI Data Mode
         */
        SPIDataMode mode = SPIDataMode::MODE0;
    };

    /**
     * @brief A virtual board mainly used for running tests
     *
     * @details The board can be manually asked what pins have what state, and pins can virtually set to receive an input.
     */
    class VirtualPinProvider : public PinProvider, public SPIProvider {
    public:
        /**
         * @brief Singleton implementation, use this static function to get the object
         */
        [[nodiscard]] static VirtualPinProvider& getInstance();

        [[nodiscard]] std::unique_ptr<Pin> getPin(int pinNumber) final;

        [[nodiscard]] std::unique_ptr<SPI> getSPI(int index) final;

        /**
         * @brief Get a mutable pin state. Used for testing.
         *
         * @param pinNumber The pin number of the pin to get
         * @return The mutable pin state
         * @throws pizzy::common::Exception If the pin doesn't exist
         */
        VirtualPinState& getPinState(int pinNumber);

        /**
         * @brief Get a mutable spi state. Used for testing.
         *
         * @param index The index of the spi to get
         * @return The mutable spi state
         * @throws pizzy::common::Exception If the spi doesn't exist
         */
        VirtualSPIState& getSPIState(int index);

    protected:
#pragma region PinProvider
        void writePin(int pinNumber, bool state) final;

        void bulkFlush(const std::vector<std::pair<int, bool>>& ops) final;

        [[nodiscard]] bool readPin(int pinNumber) final;

        void deletePin(int pinNumber) final;
#pragma endregion PinProvider

#pragma region SPIProvider
        void deleteSPI(int index) final;

        void selectSlave(int index, int slave) final;

        void setFrequency(int index, int frequency) final;

        void setBitOrder(int index, BitOrder order) final;

        void setSlaveSelectPolarity(int index, int slave, bool active) final;

        void setDataMode(int index, SPIDataMode mode) final;

        void send(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) final;

        void send(int index, const std::uint8_t* data, int length, int slave = NO_SLAVE_CHANGE) final;

        [[nodiscard]] std::vector<std::uint8_t> receive(int index, int length, int slave = NO_SLAVE_CHANGE) final;

        [[nodiscard]] std::vector<std::uint8_t> transfer(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) final;
#pragma endregion SPIProvider

    private:
        /**
         * @brief Helper function that throws if the given pin does not currently exist
         * @param pinNumber The pin number to check
         * @param caller The calling function for more helpful debugging output
         * @throws pizzy::common::Exception If the given pin doesn't exist
         */
        void checkPinExists(int pinNumber, const char* caller) const;

        /**
         * @brief Helper function that throws if the given spi does not currently exist
         * @param index The spi index to check
         * @param caller The calling function for more helpful debugging output
         * @throws pizzy::common::Exception If the given spi doesn't exist
         */
        void checkSPIExists(int index, const char* caller) const;

        /**
         * @brief Helper function that throws if the given slave is invalid
         * @param slave The slave to check
         * @param caller The calling function for more helpful debugging output
         * @throws pizzy::common::Exception If the given slave was invalid
         */
        void checkSlave(int slave, const char* caller) const;

        VirtualPinProvider();

        static std::unique_ptr<VirtualPinProvider> instance;

        std::map<int, VirtualPinState> pinStates;

        std::map<int, VirtualSPIState> spiStates;
    };

} // namespace pizzy::core
