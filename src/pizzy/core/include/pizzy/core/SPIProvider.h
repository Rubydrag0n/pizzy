/** @file */
#pragma once

#include <memory>
#include <string>
#include <unordered_set>
#include <vector>
#include <cstdint>

#include <pizzy/core/Provider.h>

namespace pizzy::core {

    class SPI;

    /**
     * @brief Constant to represent not changing the device
     * @details In the send, receive and transfer functions a slave can be set for only that specific call. If NO_SLAVE_CHANGE is given the slave will
     * not be changed for the call.
     */
    constexpr int NO_SLAVE_CHANGE = -1;

    /**
     * @brief Enum values for the different SPI Data Modes
     * @details For details see: https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html
     */
    enum class SPIDataMode {
        /** CPOL = 0, CPHA = 0 */
        MODE0,
        /** CPOL = 0, CPHA = 1 */
        MODE1,
        /** CPOL = 1, CPHA = 1 */
        MODE2,
        /** CPOL = 1, CPHA = 0 */
        MODE3
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe a string representation of the SPIDataMode into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const SPIDataMode& mode);

    /**
     * @brief Enum values for the bit order to use for SPI
     * @details Either send the highest bit first (MSB), or the lowest bit first (LSB). MSB is the default, LSB might be slow because the inverting is
     * done in software
     */
    enum class BitOrder {
        /** MSB, Send the highest bit first, default */
        MOST_SIGNIFICANT_BIT_FIRST,
        /** LSB, Send the lowest bit first */
        LEAST_SIGNIFICANT_BIT_FIRST
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe a string representation of the BitOrder into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const BitOrder& order);

    /**
     * @brief Interface for boards that have a SPI interface
     * @note Explanation of SPI: https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html
     */
    class SPIProvider : public virtual Provider {
    public:
        /**
         * @brief Get a SPI from this board
         *
         * @note Counterpart to delete SPI, implemented in the specific board
         * @param index The number of the requested SPI on the board
         * @return SPI The requested SPI
         * @throws pizzy::common::Exception If the SPI number is invalid or the SPI is already in use
         */
        [[nodiscard]] virtual std::unique_ptr<SPI> getSPI(int index) = 0;

    protected:
        SPIProvider() = default;

        /**
         * @brief Create an SPI object. This function is for the interface implementations.
         *
         * @note Counterpart to destroySPI, implemented generically in SPIProvider.
         * @param index The index of the SPI, for boards where multiple SPI can be used simultaneously.
         * @return SPI The created SPI ready to be used
         * @throws pizzy::common::Exception If the SPI is already created at that index
         */
        [[nodiscard]] std::unique_ptr<SPI> createSPI(int index);

        /**
         * @brief Deletes an SPI object. Makes pins available again. Will be called by the SPI destructor.
         *
         * @note Counterpart to createSPI, implemented generically in SPIProvider.
         * @param index The index of the SPI to delete.
         * @throws pizzy::common::Exception If the SPI does not exist.
         */
        void destroySPI(int index);

#pragma region Interface
        /**
         * @brief Delete the given SPI and make it reusable
         *
         * @note Counterpart to getSPI, implemented by the specific board
         * @param index The index of the SPI to delete
         */
        virtual void deleteSPI(int index) = 0;

        /**
         * @brief Select which slave to talk to
         *
         * @param index The index of the SPI
         * @param slave The index of the slave to select
         * @throws pizzy::common::Exception If index or the slave is invalid
         */
        virtual void selectSlave(int index, int slave) = 0;

        /**
         * @brief Set frequency of the SPI
         * @note The real frequency might be slightly off, the closest value possible is chosen
         *
         * @param index The index of the SPI
         * @param frequency The frequency to set
         * @throws pizzy::common::Exception If index is invalid
         */
        virtual void setFrequency(int index, int frequency) = 0;

        /**
         * @brief Set the bit order used
         * @note Using the non-default may be slow, because the bit order may have to be changed in software
         *
         * @param index The index of the SPI
         * @param order The bit order to use
         * @throws pizzy::common::Exception If index is invalid
         */
        virtual void setBitOrder(int index, BitOrder order) = 0;

        /**
         * @brief Sets whether the chip select pin is HIGH (true) or LOW (false) to select the pin during transfer
         *
         * @param index The index of the SPI
         * @param slave The slave for which to set
         * @param active True, if the slave select pin should be set to HIGH when transferring, False if it should be set to LOW
         * @throws pizzy::common::Exception If index is invalid
         */
        virtual void setSlaveSelectPolarity(int index, int slave, bool active) = 0;

        /**
         * @brief Sets the data mode of the SPI.
         *
         * @param index The index of the SPI
         * @param polarity Sets the clock state during idle time: true -> HIGH, false -> LOW
         * @param phase Whether to use rising or falling edge:
         * false -> sampled on falling edge and shifted on rising edge
         * true -> sampled on rising edge and shifted on falling edge
         *
         * @note For more information see https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html
         * @note polarity is the CPOL bit, phase is the CPHA bit
         * @throws pizzy::common::Exception If index is invalid
         */
        void setDataMode(int index, bool polarity, bool phase);

        /**
         * @brief Sets the clock mode of the SPI.
         *
         * @param index The index of the SPI
         * @param mode The data mode to use
         */
        virtual void setDataMode(int index, SPIDataMode mode) = 0;

        /**
         * @brief Send the given data bytewise through the given SPI
         *
         * @param index The index of the SPI
         * @param data The data, given as a vector of bytes
         * @param slave The slave to send to. By default the currently selected slave will be used.
         * @throws pizzy::common::Exception If invalid index or slave is given
         */
        virtual void send(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) = 0;

        /**
         * @brief Send the given data bytewise through the given SPI
         *
         * @param index The index of the SPI
         * @param data The data, given as a c array of bytes
         * @param length The length of the given array
         * @param slave The slave to send to. By default the currently selected slave will be used.
         * @throws pizzy::common::Exception If invalid index, slave or length is given
         */
        virtual void send(int index, const std::uint8_t* data, int length, int slave = NO_SLAVE_CHANGE) = 0;

        /**
         * @brief Read a given number of bytes from the SPI
         * @note As it's only possible to read from SPI while sending, this function will send the given number of bytes of zeros to receive
         *
         * @param index The index of the SPI
         * @param length The number of bytes that shall be received
         * @param slave The slave to reveive from. By default the currently selected slave will be used.
         * @returns The received data
         * @throws pizzy::common::Exception If invalid index or slave is given
         */
        [[nodiscard]] virtual std::vector<std::uint8_t> receive(int index, int length, int slave = NO_SLAVE_CHANGE) = 0;

        /**
         * @brief Transfer the given data to the SPI and simultaneously read and return the received data
         *
         * @param index The index of the SPI
         * @param data The data to send
         * @param slave The slave to transfer to and from. By default the currently selected slave will be used.
         * @returns The received data
         * @throws pizzy::common::Exception If invalid index or slave is given
         */
        [[nodiscard]] virtual std::vector<std::uint8_t> transfer(int index, const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE) = 0;
#pragma endregion Interface

    private:
        friend SPI;

        /**
         * @brief Keeps track of registered spis
         */
        std::unordered_set<int> spis;
    };

} // namespace pizzy::core
