/** @file */
#pragma once

#include <cstdint>

#include <pizzy/core/SPIProvider.h>

namespace pizzy::core {

    /**
     * @brief SPI object, bound to a board and index.
     * @details Multiple of these can exist and be instantiated on the same board at the same time, if the board has multiple SPIs
     */
    class SPI {
    public:
        virtual ~SPI();

        // Disallow copying or moving
        SPI(const SPI&) = delete;
        SPI(SPI&&) = delete;

        SPI& operator=(const SPI&) = delete;
        SPI& operator=(SPI&&) = delete;

        /**
         * @brief Get this SPI's index
         *
         * @return int The index of this SPI
         */
        [[nodiscard]] int getIndex() const;

        /**
         * @brief Get the name of this SPI
         *
         * @return std::string A string containing the name
         */
        [[nodiscard]] std::string getName() const;

        /**
         * @brief Select which slave to talk to
         *
         * @param slave The index of the slave to select
         * @throws pizzy::common::Exception If the slave is invalid
         */
        void selectSlave(int slave);

        /**
         * @brief Set frequency of this SPI
         * @note The real frequency might be slightly off, the closest value possible is chosen
         *
         * @param frequency The frequency to set
         */
        void setFrequency(int frequency);

        /**
         * @brief Set the bit order used
         * @note Using the non-default may be slow, because the bit order may have to be changed in software
         *
         * @param order The bit order to use
         */
        void setBitOrder(BitOrder order);

        /**
         * @brief Sets whether the chip select pin is HIGH (true) or LOW (false) to select the pin during transfer
         *
         * @param slave The slave for which to set the polarity
         * @param active True, if the slave select pin should be set to HIGH when transferring, False if it should be set to LOW
         */
        void setSlaveSelectPolarity(int slave, bool active);

        /**
         * @brief Sets the data mode of this SPI
         *
         * @param polarity Sets the clock state during idle time: true -> HIGH, false -> LOW
         * @param phase Whether to use rising or falling edge:
         * false -> sampled on falling edge and shifted on rising edge
         * true -> sampled on rising edge and shifted on falling edge
         *
         * @note For more information see https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html
         * @note polarity is the CPOL bit, phase is the CPHA bit
         */
        void setDataMode(bool polarity, bool phase);

        /**
         * @brief Sets the clock mode of this SPI.
         *
         * @param mode The data mode to use
         */
        void setDataMode(SPIDataMode mode);

        /**
         * @brief Send the given data bytewise through this SPI
         *
         * @param data The data, given as a vector of bytes
         * @param slave The slave to send to. By default the currently selected slave will be used. Will return to the previously selected slave after
         * the call is finished
         * @throws pizzy::common::Exception If invalid slave is given
         */
        void send(const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE);

        /**
         * @brief Send the given data bytewise through this SPI
         *
         * @param data The data, given as a c array of bytes
         * @param length The length of the given array
         * @param slave The slave to send to. By default the currently selected slave will be used. Will return to the previously selected slave after
         * the call is finished
         * @throws pizzy::common::Exception If invalid slave or length is given
         */
        void send(const std::uint8_t* data, int length, int slave = NO_SLAVE_CHANGE);

        /**
         * @brief Read a given number of bytes from the SPI
         * @note As it's only possible to read from SPI while sending, this function will send the given number of bytes of zeros to receive
         *
         * @param length The number of bytes that shall be received
         * @param slave The slave to reveive from. By default the currently selected slave will be used. Will return to the previously selected slave
         * after the call is finished
         * @returns The received data
         * @throws pizzy::common::Exception If invalid slave is given
         */
        [[nodiscard]] std::vector<std::uint8_t> receive(int length, int slave = NO_SLAVE_CHANGE);

        /**
         * @brief Transfer the given data to the SPI and simultaneously read and return the received data
         *
         * @param data The data to send
         * @param slave The slave to transfer to and from. By default the currently selected slave will be used. Will return to the previously
         * selected slave after the call is finished
         * @returns The received data
         * @throws pizzy::common::Exception If invalid slave is given
         */
        [[nodiscard]] std::vector<std::uint8_t> transfer(const std::vector<std::uint8_t>& data, int slave = NO_SLAVE_CHANGE);

    protected:
        /**
         * @brief Create the SPI on a given index and provider
         *
         * @param index The index of the SPI to create
         * @param provider The provider to create the SPI on
         * @throws pizzy::common::Exception If the index is invalid
         */
        SPI(int index, SPIProvider& provider);

    private:
        friend SPIProvider;

        int index;

        SPIProvider& spiProvider;
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe the toString() into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const SPI& spi);

} // namespace pizzy::core
