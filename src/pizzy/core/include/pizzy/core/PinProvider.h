/** @file */
#pragma once

#include <pizzy/core/Provider.h>

#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

namespace pizzy::core {

    class Pin;

    /**
     * @brief Any device that has controllable pins
     */
    class PinProvider : public virtual Provider {
    public:

        virtual ~PinProvider();

        /**
         * @brief Get a pin from this board
         *
         * @note Counterpart to deletePin, implemented in the specific board
         * @param pinNumber The number of the requested Pin
         * @return Pin The requested Pin
         * @throws pizzy::common::Exception If the pin number is invalid or the Pin is already in use
         */
        [[nodiscard]] virtual std::unique_ptr<Pin> getPin(int pinNumber) = 0;

    protected:
        PinProvider();

        /**
         * @brief Create a Pin object. This function is for the interface implementations
         *
         * @note Counterpart to destroyPin, implemented generically in PinProvider
         * @param pinNumber The number of the pin to be created.
         * @return Pin The created Pin ready to be used
         * @throws pizzy::common::Exception If the pin is already created
         */
        [[nodiscard]] std::unique_ptr<Pin> createPin(int pinNumber);

        /**
         * @brief Destroys a pin object. This will remove the pin from registered pins on this board.
         *
         * @note Counterpart to createPin, implemented generically in PinProvider
         * @param pinNumber The number of the pin to be destroyed.
         * @throws pizzy::common::Exception If the pin was not created before
         */
        void destroyPin(int pinNumber);

        /**
         * @brief Calls bulkFlush on every registered PinProvider
         */
        static void bulkFlushAll();

        /**
         * @brief Part of a bulkWrite operation, does not immediately set the pin. See Pin::bulkWrite
         *
         * @param pinNumber The pin number to set
         * @param state The state to set the pin to
         */
        virtual void bulkWrite(int pinNumber, bool state);

#pragma region Interface
        /**
         * @brief Set the given pin to the given state.
         *
         * @param pinNumber The pin number to set
         * @param state The state to set the pin to
         */
        virtual void writePin(int pinNumber, bool state) = 0;

        /**
         * @brief Apply all given write operations.
         * 
         * @param ops A list of pairs of pin numbers and the state they should be set to.
         */
        virtual void bulkFlush(const std::vector<std::pair<int, bool>>& ops) = 0;

        /**
         * @brief Read the given pin
         *
         * @param pinNumber The number of the pin to read
         * @return bool True if the pin is set to high, false if the pin is set to low
         */
        [[nodiscard]] virtual bool readPin(int pinNumber) = 0;

        /**
         * @brief Delete the given pin and make it reusable
         *
         * @note Counterpart to getPin, implemented by the specific board
         * @param pinNumber The number of the pin to delete
         */
        virtual void deletePin(int pinNumber) = 0;
#pragma endregion Interface

        const std::vector<std::pair<int, bool>>& getBulkOperations() const;

        void clearBulkOperations();

    private:
        friend Pin;

        /**
         * @brief Keeps track of registered pins
         */
        std::unordered_set<int> pins;

        /**
         * @brief A vector keeping track of bulk operations since last bulkFlush
         */
        std::vector<std::pair<int, bool>> bulkOperations;

        /**
         * @brief A registry of all existing pin providers
         */
        static std::vector<PinProvider*> pinProviderRegistry;
    };

} // namespace pizzy::core
