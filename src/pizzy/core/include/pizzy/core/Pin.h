/** @file */
#pragma once

#include <ostream>
#include <string>
#include <vector>

namespace pizzy::core {
    class PinProvider;

    /**
     * @brief The Pin class. Represents a physical pin bound to a port
     */
    class Pin {
    public:
        /**
         * @brief Destructs a pin, sets low and mode to INPUT
         */
        ~Pin();

        // Disallow copying or moving
        Pin(const Pin&) = delete;
        Pin(Pin&&) = delete;

        Pin& operator=(const Pin&) = delete;
        Pin& operator=(Pin&&) = delete;

        /**
         * @brief Returns the pin number
         *
         * @returns The pin number
         */
        [[nodiscard]] int getPinNumber() const;

        /**
         * @brief Get string describing the pin, consisting of short board name and pin number
         *
         * @return The string
         */
        [[nodiscard]] std::string getName() const;

        /**
         * @brief Sets the Pin to the desired value, false is low, true is high
         *
         * @param state The state to set. "true" sets the pin to high, false to low
         */
        void write(bool state) const;

        /**
         * @brief Write to this Pin as part of a bulk write.
         * @details This function can be called on multiple pins before calling bulkFlush() to apply all the pin changes simultaneously. This is
         * advantageous for example when the pins are provided by a shift register. This way, all shift register output pins can be set in the same
         * operation instead of many individual ones.
         *
         * @note This is *not* immediately applied! Call Pin::bulkFlush to flush every unapplied bulkWrite call.
         * @param state The state to set. "true" sets the pin to high, false to low
         */
        void bulkWrite(bool state) const;

        static void bulkFlush();

        /**
         * @brief Reads the current Pin value
         *
         * @returns "true" when the pin is high, "false" if the pin is low
         */
        [[nodiscard]] bool read() const;

    protected:
        /**
         * @brief Initializes a pin with given number
         */
        Pin(int pin, PinProvider& pinProvider);

    private:
        int pin;

        PinProvider& pinProvider;

        friend PinProvider;
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe the toString() into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const Pin& pin);

} // namespace pizzy::core
