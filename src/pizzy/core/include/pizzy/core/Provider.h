/** @file */
#pragma once

#include <string>

namespace pizzy::core {

    /**
     * @brief Base class for specific providers like PinProvider or SPIProvider
     */
    class Provider {
    public:
        /**
         * @brief Destruct the Provider
         */
        virtual ~Provider() = default;

        // Disallow copying or moving
        Provider(const Provider&) = delete;
        Provider(Provider&&) = delete;

        Provider& operator=(const Provider&) = delete;
        Provider& operator=(Provider&&) = delete;

        /**
         * @brief Get the name of this provider
         *
         * @return std::string A string containing the name
         */
        [[nodiscard]] std::string getName() const;

        /**
         * @brief Get short name of the provider, e.g. "BCM" or "VIRT"
         *
         * @return std::string A string containing the short name
         */
        [[nodiscard]] std::string getShortName() const;

    protected:
        /**
         * @brief Construct a provider, giving it a name and abbreviated name
         *
         * @param name A full name for the Provider, e.g. "Virtual Board"
         * @param shortName A short name for the Provider, e.g. "VIRT"
         */
        Provider(std::string name, std::string shortName);

    private:

        const std::string name;

        const std::string shortName;
    };

} // namespace pizzy::core
