/** @file */
#pragma once

#include <pizzy/common/Color.h>
#include <pizzy/core/SPI.h>
#include <pizzy/peripherals/Peripheral.h>

#include <string>
#include <tuple>
#include <vector>

namespace pizzy::peripherals {

    /**
     * @brief The flushing mode of a device.
     */
    enum class FlushMode {
        /** The user has to call flush for an update to be flushed onto the device */
        MANUAL,
        /** A flush will automatically happen after each change */
        AUTO
    };

    /**
     * @brief A config describing a WS2812B Device
     * @details This class is used to create WS2812B devices. Example:
     * @code
     * WS2812B device(WS2812BConfig{.spi = board.getSPI(0), .leds = 12});
     * @endcode
     */
    struct WS2812BConfig {

        /**
         * @brief The Interface the WS2812B is controlled from
         */
        std::unique_ptr<pizzy::core::SPI> spi;

        /**
         * @brief Number of LEDs to be controlled
         */
        std::size_t leds;

        /**
         * @brief The flushing mode of the WS2812B device. Default AUTO. See FlushMode
         */
        FlushMode flushMode = FlushMode::AUTO;
    };

    /**
     * @example RGBRing/RGBRingDemo.cpp
     */

    /**
     * @brief Implements the WS2812B protocol
     * @details This class represents a physically existing WS2812B device
     */
    class WS2812B : public Peripheral {
    public:
        /**
         * @brief Deconstruct the device
         */
        virtual ~WS2812B() override = default;

        /**
         * @brief Constructs the Device with the given config
         * @details An interface must be valid, otherwise undefined behaviour will occur
         */
        WS2812B(WS2812BConfig&& config);

        /**
         * @brief Get a string representation of the device.
         *
         * @return The string describing the device and its state
         */
        [[nodiscard]] std::string toString() const override;

        /**
         * @brief Will flush the current state to the device. If the device is in FlushMode::AUTO this will have no effect.
         */
        void flush();

        /**
         * @brief Sets the color of an LED at the given index
         * @note If the device is in FlushMode::AUTO the device will immediately update after this call
         *
         * @param index The index at which to set the color
         * @param color The color to set
         */
        void setColor(int index, common::Color color);

        /**
         * @brief Sets the color of all LEDs in the vector
         *
         * @param colors The colors to set. A vector of pairs, where the pair consists of the index of the LED and the color for that index
         */
        void setColor(std::vector<std::pair<std::size_t, common::Color>> colors);

        /**
         * @brief Sets the color of every LED in the vector, first LED to n-th LED
         *
         * @param colors The colors to set. Frist LED will be set to the first color in the vector, and so on.
         * @throws pizzy::common::Exception If the vector has more entries than the device has LEDs
         */
        void setColor(std::vector<common::Color> colors);

        /**
         * @brief Returns whether the object has changed since the last flush to device
         *
         * @return True if the object state is the same as device state
         */
        bool isFlushed() const;

    protected:
    private:
        /**
         * @brief The config for the class
         */
        const WS2812BConfig config;

        /**
         * @brief A flag. True when the device and the object are in the same state, False if the object has changed since the last flush
         */
        bool flushed;

        /**
         * @brief Internal state
         */
        std::vector<common::Color> state;
    };

} // namespace pizzy::peripherals
