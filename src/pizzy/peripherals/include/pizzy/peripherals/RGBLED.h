/** @file */
#pragma once

#include <pizzy/common/Color.h>
#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/Peripheral.h>

#include <memory>

namespace pizzy::peripherals {

    /**
     * @brief The type of a RGBLED
     * @details A RGB LED has 4 pins, 1 for red, green and blue each, and one last pin that is either the common anode or common cathode of the other
     * 3. Common Anode means that that fourth pin should be connected to GND, while Common Cathode means that the fourth pin should be connected to a
     * voltage source.
     */
    enum class RGBLEDTYPE {
        COMMON_ANODE,
        COMMON_CATHODE,
    };

    /**
     * @brief The 8 possible basic colors of a RGBLED
     */
    enum class COLOR {
        BLACK = 0b000,
        RED = 0b100,
        GREEN = 0b010,
        BLUE = 0b001,
        YELLOW = RED | GREEN, // thanks appels
        PURPLE = RED | BLUE,
        TURQUOISE = GREEN | BLUE,
        WHITE = 0b111
    };

    /**
     * @brief A config describing a RGBLED
     * @details This class is used to create RGBLEDs. Example:
     * @code
     * RGBLED rgbled(RGBLEDConfig{.red = bcm.getPin(14), .green = bcm.getPin(15), .blue = bcm.getPin(18), .type = RGBLEDType::COMMON_CATHODE});
     * @endcode
     */
    struct RGBLEDConfig {
        /**
         * @brief The pin connected to the "red" pin of the LED
         */
        std::unique_ptr<core::Pin> red;

        /**
         * @brief The pin connected to the "green" pin of the LED
         */
        std::unique_ptr<core::Pin> green;

        /**
         * @brief The pin connected to the "blue" pin of the LED
         */
        std::unique_ptr<core::Pin> blue;

        /**
         * @brief The type of the RGBLED, see RGBLEDType
         */
        RGBLEDTYPE type;
    };

    /**
     * @example RGBLed/RGBLEDDemo.cpp
     */

    /**
     * @brief A class representing a physical RGBLED
     * @note Take care to use appropriate resistors to protect the LED from breaking. This can help you:
     * https://ohmslawcalculator.com/led-resistor-calculator
     * @details A RGB LED is a LED with 4 pins: 1 for red, green and blue each, and one common anode/cathode (see RGBLEDType). This class can set a
     * RGBLED to the 8 trivially representable colors. See RGBLEDDemo for an Example
     */
    class RGBLED : public Peripheral {
    public:
        /**
         * @brief Create a RGBLED from its RGBLEDConfig
         * @param config The Config to use
         */
        explicit RGBLED(RGBLEDConfig&& config);

        /**
         * @brief Destructs the RGBLED, turning the LED off
         */
        ~RGBLED() override;

        /**
         * @brief Sets the LED to the given color
         *
         * @param color The color to set
         */
        void set(COLOR color);

        /**
         * @brief Returns a string representation of the LED and its current status
         */
        [[nodiscard]] std::string toString() const override;

    protected:
    private:
        RGBLEDConfig config;

        COLOR lastColor;
    };

} // namespace pizzy::peripherals
