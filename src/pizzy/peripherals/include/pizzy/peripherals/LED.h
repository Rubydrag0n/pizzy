/** @file */
#pragma once

#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/Peripheral.h>

#include <memory>

using namespace pizzy::core;

namespace pizzy::peripherals {

    /**
     * @brief The LED Type
     * @details Whether the connected pin is the cathode (negative) or anode (positive) of the LED
     */
    enum class LEDType {
        /** Type Cathode, the connected pin should be the negative end, the other should be connected to a voltage source */
        CATHODE,
        /** Type Anode, the connected pin should be the positive end, the other should be connected to GND */
        ANODE
    };

    /**
     * @brief A config describing a LED
     * @details This class is used to create LEDs. Example:
     * @code
     * LED led(LEDConfig{.pin = board.getPin(14)});
     * @endcode
     */
    struct LEDConfig {

        /**
         * @brief The pin the LED is controlled from
         * @details One pin of the LED should be connected to this GPIO, while the other should be connected to either GROUND or POWER, depending on
         * the LEDType set.
         */
        std::unique_ptr<Pin> pin;

        /**
         * @brief Whether the connected pin is the cathode (negative) or anode (positive) of the LED
         */
        LEDType type = LEDType::ANODE;

        /**
         * @brief The default state of the LED; will be set on construction and destruction of the object
         */
        bool defaultState = false;
    };

    /**
     * @brief A class representing a physical LED
     * @note Take care to use appropriate resistors to protect the LED from breaking. This can help you:
     * https://ohmslawcalculator.com/led-resistor-calculator
     * @details This class represents a physically existing LED on a given pin. Both Anode and Cathode LEDs are supported and the default state can be
     * set. For more information on the available configuration see LEDConfig.
     */
    class LED final : public Peripheral {
    public:
        /**
         * @brief Create a new LED with the given LEDConfig
         * @details Pin must be a valid object, otherwise undefined behaviour will occur
         * @throws pizzy::common::Exception If an invalid type was given
         */
        explicit LED(LEDConfig&& config);

        ~LED() override;

        /**
         * @brief Sets the LED to the given state
         *
         * @param state The state to set. True turns the LED on, false turns it off
         */
        void setState(bool state);

        /**
         * @brief Returns whether the LED is currently turned on or off
         *
         * @returns The state of the LED
         */
        [[nodiscard]] bool getState() const;

        /**
         * @brief Returns a string representation of the LED and its current status
         */
        [[nodiscard]] std::string toString() const override;

    protected:
    private:
        const LEDConfig config;

        bool state;
    };

} // namespace pizzy::peripherals
