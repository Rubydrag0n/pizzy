/** @file */
#pragma once

#include <pizzy/common/Utility.h>
#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/Peripheral.h>

#include <chrono>
#include <memory>

namespace pizzy::peripherals {

    /**
     * @brief The config for a Relay
     * @details This class is used to create Relays. Example:
     * @code
     * Relay relay(RelayConfig{.pin = board.getPin(14)});
     * @endcode
     */
    struct RelayConfig {

        /**
         * @brief The pin the Relay is controlled from
         */
        std::unique_ptr<core::Pin> pin;

        /**
         * @brief The default state of the Relay
         * @details This state will be set on construction and reset to this when destroying
         * @note If the relay is inverted, this will be treated like any other input value and thus also be inverted
         */
        bool defaultState = false;

        /**
         * @brief Whether the relay is inverted
         * @details If this is true the pin control will be inverted. Setting the relay
         * to true will set the pin low and vice versa. Asking the Relay's state
         * will return "true" when its pin is turned off and vice versa. In essence, the
         * inversion should be invisible to the user of this class after construction
         */
        bool inverted = false;

        /**
         * @brief The time that has to have passed before allowing a switch again
         */
        pizzy::common::clock::duration minTimeBetweenSwitches = std::chrono::seconds(2);
    };

    /**
     * @brief Class describing a relay
     * @details The class represents a physical relay. The default state can be set, the relay can be inverted and it is asserted that a minimum time
     * has passed between switches, as relais can be damaged when switched too quickly. For Configuration options see RelayConfig
     */
    class Relay final : public Peripheral {
    public:
        /**
         * @brief Create a relay from the given RelayConfig
         * @details Pin must be a valid object, otherwise undefined behaviour will occur
         *
         * @param config The RelayConfig to use for creation
         */
        explicit Relay(RelayConfig&& config);

        /**
         * @brief Destruct the object
         * @note This will return to the given defaultState **without** checking time passed since last switch.
         */
        ~Relay();

        /**
         * @brief Set the Relay to a state
         * @details Given "true" will switch the Relay "on", and "off" if given false, taking into account if the Relay is inverted
         *
         * @param state The state to set
         */
        void set(bool state);

        /**
         * @brief Returns whether the Relay is currently on or off
         */
        [[nodiscard]] bool getState() const;

        /**
         * @brief Returns a string representation of the Relay's current state
         *
         * @return A string containing the Relay's state
         */
        [[nodiscard]] std::string toString() const override;

    protected:
        /**
         * @brief Checks whether enough time has passed since this Relay was last switched
         *
         * @return true if it is safe to switch again
         */
        [[nodiscard]] bool safeToSwitch() const;

    private:
        RelayConfig config;

        /**
         * @brief The actual state of the pin
         */
        bool state;

        pizzy::common::clock::time_point lastUse;
    };

} // namespace pizzy::peripherals
