#pragma once

#include <pizzy/core/Pin.h>
#include <pizzy/core/PinProvider.h>
#include <pizzy/core/SPIProvider.h>

#include <memory>
#include <vector>

namespace pizzy::peripherals {

    /**
     * @brief A config describing ShiftRegister
     * @details This class is used to create Shift Registers. Example:
     * @code
     * ShiftRegister shiftReg(ShiftRegisterConfig{.data = bcm.getPin(14), .clock = bcm.getPin(15), .latch = bcm.getPin(18));
     * @endcode
     */
    struct ShiftRegisterConfig {
        /**
         * @brief The pin connected to the "data"/"SER"/"Serial Input" of the shift register
         */
        std::unique_ptr<core::Pin> data;

        /**
         * @brief The pin connected to the "shift register clock"/"SRCLK" of the shift register
         */
        std::unique_ptr<core::Pin> clock;

        /**
         * @brief The pin connected to the "latch"/"RCLK" of the shift register
         */
        std::unique_ptr<core::Pin> latch;

        /**
         * @brief The pin connected to the "clear"/"SRCLR" of the shift register (optional, can connect to VCC)
         */
        std::unique_ptr<core::Pin> clear = nullptr;

        /**
         * @brief The pin connected to the "output enable"/"OE" of the shift register (optional, can connect to GND)
         */
        std::unique_ptr<core::Pin> enable = nullptr;

        /**
         * @brief The number of output pins of the shift register (default: 8)
         */
        unsigned size = 8;
    };

    /**
     * @example ShiftRegister/ShiftRegisterDemo.cpp
     */

    /**
     * @brief A Serial In - Parallel Out Shift Register
     * @details This implements the PinProvider interface, so the output pins can be used without knowing they are behind a Shift Register.
     * Connect its GND pin to GND and VCC to 3.3V
     */
    class ShiftRegister : public core::PinProvider {
    public:
        /**
         * @brief Create a ShiftRegister from its ShiftRegisterConfig
         * @param config The Config to use
         */
        explicit ShiftRegister(ShiftRegisterConfig&& config);

        /**
         * @brief Destructs the ShiftRegister, turning all outputs off
         */
        ~ShiftRegister();

        [[nodiscard]] std::unique_ptr<core::Pin> getPin(int pinNumber) final;

    protected:
#pragma region PinProvider
        virtual void writePin(int pinNumber, bool state) final;

        void bulkFlush(const std::vector<std::pair<int, bool>>& ops) final;

        [[nodiscard]] virtual bool readPin(int pinNumber) final;

        virtual void deletePin(int pinNumber) final;
#pragma endregion PinProvider

        /**
         * @brief Clears all output pins to zero
         */
        void clear();

        /**
         * @brief Sets the output pins of the shift register to the values in registerValues
         */
        void set();

        /**
         * @brief Publishes the shift register stored values to the actual outputs
         */
        void publish();

    private:
        ShiftRegisterConfig config;

        std::vector<bool> registerValues;
    };

} // namespace pizzy::peripherals
