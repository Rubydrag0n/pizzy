/** @file */
/*#pragma once

#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/Peripheral.h>

#include <string>

using namespace std::string_literals;

namespace pizzy::peripherals {

    struct ServoDefinition {};

    const std::string SERVOBLASTER_FILE_PATH = "/dev/servoblaster"s;

    class Servo : public Peripheral {
    public:
        // Initializes the Servo on the given pin with the given parameters for the
        // servo
        explicit Servo(const pizzy::proto::Servo& servo);

        Servo(const Servo&) = delete;
        Servo(Servo&&) = default;

        Servo& operator=(const Servo&) = delete;
        Servo& operator=(Servo&&) = default;

        // Moves to connfigured home position
        void moveHome() const;

        // Sets target duty cycle to the specified value from between 0 and 1
        void setCycle(double value) const;

        // Returns the currently active duty cycle
        [[nodiscard]] double getCycle() const;

    protected:
    private:
        const int pin;
        const double minDutyCycle;
        const double maxDutyCycle;

        const double homePosition;

        mutable double currentCycle;
    };

} // namespace pizzy::peripherals
*/