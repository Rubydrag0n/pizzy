/** @file */
#pragma once

#include <ostream>
#include <string>

namespace pizzy::peripherals {

    /**
     * @brief Base class of every peripheral
     * @details A peripheral will generally take a specific PeripheralConfig to be constructed, e.g. a LEDConfig to construct a LED object.
     * Peripherals are hardware agnostic, which means they only get their pins through which they control the physical peripheral, without knowing
     * what pin it is or on what board.
     */
    class Peripheral {
    public:
        /**
         * @brief Create a peripheral
         * @param name The name of the peripheral
         */
        Peripheral(std::string name);

        /**
         * @brief Destructs the peripheral
         */
        virtual ~Peripheral() = default;

        /**
         * @brief Returns the peripheral's name
         */
        [[nodiscard]] std::string getName() const;

        /**
         * @brief Returns some string describing this peripheral
         */
        [[nodiscard]] virtual std::string toString() const = 0;

    protected:
    private:
        std::string name;
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe the toString() into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const Peripheral& peripheral);

} // namespace pizzy::peripherals
