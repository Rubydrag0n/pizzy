#include <pizzy/common/Exception.h>
#include <pizzy/peripherals/RGBLED.h>

using namespace pizzy::core;
using namespace pizzy::peripherals;

namespace {
    std::string typeToString(const RGBLEDTYPE type) {
        switch (type) {
        case RGBLEDTYPE::COMMON_ANODE:
            return "common anode";
        case RGBLEDTYPE::COMMON_CATHODE:
            return "common cathode";
        default:
            return "unknown";
        }
    }

    std::string colorToString(const COLOR color) {
        switch (color) {
        case COLOR::BLACK:
            return "black";
        case COLOR::RED:
            return "red";
        case COLOR::GREEN:
            return "green";
        case COLOR::BLUE:
            return "blue";
        case COLOR::YELLOW:
            return "yellow";
        case COLOR::PURPLE:
            return "purple";
        case COLOR::TURQUOISE:
            return "turqoise";
        case COLOR::WHITE:
            return "white";
        default:
            return "unknown";
        }
    }
} // namespace

namespace pizzy::peripherals {

    RGBLED::RGBLED(RGBLEDConfig&& config)
        : Peripheral(typeToString(config.type) + "-RGBLED at " + config.red->getName() + ", " + config.green->getName() + " and " +
                     config.blue->getName())
        , config(std::move(config)) {
        if (typeToString(this->config.type) == "unknown") {
            throw common::Exception("LED Type was 'unknown', can't create LED " + getName());
        }
        set(COLOR::BLACK);
    }

    RGBLED::~RGBLED() {
        set(COLOR::BLACK);
    }

    void RGBLED::set(const COLOR color) {
        std::uint8_t colorint = static_cast<std::uint8_t>(color);
        if (config.type == RGBLEDTYPE::COMMON_ANODE) {
            // invert bits
            colorint ^= -1;
        }

        config.red->write(colorint & int(COLOR::RED));
        config.green->write(colorint & int(COLOR::GREEN));
        config.blue->write(colorint & int(COLOR::BLUE));

        lastColor = color;
    }

    std::string RGBLED::toString() const {
        return getName() + " color " + colorToString(lastColor);
    }

} // namespace pizzy::peripherals
