#include <pizzy/peripherals/Peripheral.h>

namespace pizzy::peripherals {

    Peripheral::Peripheral(std::string name)
        : name(std::move(name)) {
    }

    std::string Peripheral::getName() const {
        return name;
    }

    std::ostream& operator<<(std::ostream& os, const Peripheral& peripheral) {
        os << peripheral.toString();
        return os;
    }

} // namespace pizzy::peripherals
