#include <pizzy/common/Exception.h>
#include <pizzy/common/Utility.h>
#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/Peripheral.h>
#include <pizzy/peripherals/Relay.h>

#include <chrono>

namespace pizzy::peripherals {

    Relay::Relay(RelayConfig&& config)
        : Peripheral("Relay at " + config.pin->getName())
        , config(std::move(config))
        , state(this->config.pin->read()) {
        set(config.defaultState);
    }

    Relay::~Relay() {
        // force back to default
        config.minTimeBetweenSwitches = std::chrono::milliseconds(0);
        set(config.defaultState);
    }

    void Relay::set(const bool value) {
        const bool valueToSet = value != config.inverted; // XOR

        if (state == valueToSet) {
            // nothing to be done
            return;
        }

        auto now = common::clock::now();

        if (safeToSwitch()) {
            state = valueToSet;
            config.pin->write(state);
            lastUse = common::clock::now();
        } else {
            throw common::Exception("Trying to switch Relay too quickly, not switching. Last switch was " +
                                    common::Utility::timeToString(now - lastUse) + " ago");
        }
    }

    std::string Relay::toString() const {
        return getName() + " turned " + (getState() ? "on" : "off");
    }

    bool Relay::getState() const {
        return state != config.inverted;
    }

    bool Relay::safeToSwitch() const {
        const auto now = common::clock::now();
        if (now - lastUse < config.minTimeBetweenSwitches) {
            return false;
        }
        return true;
    }

} // namespace pizzy::peripherals
