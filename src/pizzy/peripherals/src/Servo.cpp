/*#include <pizzy/peripherals/Servo.h>

namespace pizzy::peripherals {

    Servo::Servo(const proto::Servo& servo)
        : Peripheral(servo.name())
        , pin(servo.pin().pinnumber())
        , minDutyCycle(servo.mindutycycle())
        , maxDutyCycle(servo.maxdutycycle())
        , homePosition(servo.home().percentage())
        , currentCycle(0) {
        moveHome();
    }

    void Servo::moveHome() const {
        setCycle(homePosition);
    }

    void Servo::setCycle(double value) const {
        if (value > 1) {
            value = 1;
        } else if (value < 0) {
            value = 0;
        }

        currentCycle = value;

        int dutyCycle = minDutyCycle + (maxDutyCycle - minDutyCycle) * value;
        std::ofstream servoblaster(SERVOBLASTER_FILE_PATH, std::ios_base::out | std::ios_base::app);
        if (servoblaster.is_open()) {
            servoblaster << pin << "=" << dutyCycle << std::endl;
        } else {
            // common::Logger::logWarning("Couldn't open file at ", SERVOBLASTER_FILE_PATH, " to control servos. Is servoblaster running?");
        }
    }

    double Servo::getCycle() const {
        return currentCycle;
    }

} // namespace pizzy::peripherals
*/