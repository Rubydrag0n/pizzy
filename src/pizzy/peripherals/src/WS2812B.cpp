#include <pizzy/common/Color.h>
#include <pizzy/common/Exception.h>
#include <pizzy/core/SPI.h>
#include <pizzy/peripherals/WS2812B.h>

#include <sstream>
#include <string>
#include <vector>

namespace {

    constexpr uint32_t FREQUENCY = 2'400'000;
    constexpr std::size_t BYTE_PER_LED = 9;

    inline constexpr unsigned char operator"" _u8(unsigned long long arg) noexcept {
        return static_cast<unsigned char>(arg);
    }

    inline void setData(std::vector<std::uint8_t>& data, std::size_t index, const pizzy::common::Color& color) {
        // this is where the bits go:         8  7  6
        constexpr std::uint8_t byte1mask = 0b10010010;
        //                                     5  4
        constexpr std::uint8_t byte2mask = 0b01001000;
        //                                    3  2  1
        constexpr std::uint8_t byte3mask = 0b10010010;

        const std::uint8_t& r = color.r;
        const std::uint8_t& g = color.g;
        const std::uint8_t& b = color.b;

        data[index * BYTE_PER_LED + 0] = byte1mask | ((g & (1_u8 << 7)) >> 1) | ((g & (1_u8 << 6)) >> 3) | ((g & (1_u8 << 5)) >> 5);
        data[index * BYTE_PER_LED + 1] = byte2mask | ((g & (1_u8 << 4)) << 1) | ((g & (1_u8 << 3)) >> 1);
        data[index * BYTE_PER_LED + 2] = byte3mask | ((g & (1_u8 << 2)) << 4) | ((g & (1_u8 << 1)) << 2) | ((g & (1_u8 << 0)) << 0);
        data[index * BYTE_PER_LED + 3] = byte1mask | ((r & (1_u8 << 7)) >> 1) | ((r & (1_u8 << 6)) >> 3) | ((r & (1_u8 << 5)) >> 5);
        data[index * BYTE_PER_LED + 4] = byte2mask | ((r & (1_u8 << 4)) << 1) | ((r & (1_u8 << 3)) >> 1);
        data[index * BYTE_PER_LED + 5] = byte3mask | ((r & (1_u8 << 2)) << 4) | ((r & (1_u8 << 1)) << 2) | ((r & (1_u8 << 0)) << 0);
        data[index * BYTE_PER_LED + 6] = byte1mask | ((b & (1_u8 << 7)) >> 1) | ((b & (1_u8 << 6)) >> 3) | ((b & (1_u8 << 5)) >> 5);
        data[index * BYTE_PER_LED + 7] = byte2mask | ((b & (1_u8 << 4)) << 1) | ((b & (1_u8 << 3)) >> 1);
        data[index * BYTE_PER_LED + 8] = byte3mask | ((b & (1_u8 << 2)) << 4) | ((b & (1_u8 << 1)) << 2) | ((b & (1_u8 << 0)) << 0);
    }
} // namespace

namespace pizzy::peripherals {

    WS2812B::WS2812B(WS2812BConfig&& conf)
        : Peripheral("WS2812B at " + conf.spi->getName())
        , config(std::move(conf))
        , flushed(false)
        , state() {
        config.spi->setFrequency(FREQUENCY);
        config.spi->setBitOrder(core::BitOrder::MOST_SIGNIFICANT_BIT_FIRST);

        // initialize
        state.resize(config.leds, common::Color{.r = 0, .g = 0, .b = 0});

        if (config.flushMode == FlushMode::AUTO) {
            flush();
        }
    }

    std::string WS2812B::toString() const {
        std::ostringstream os;
        os << getName() << " with " << config.leds << " LEDs, currently " << (flushed ? "flushed" : "not flushed");
        return os.str();
    }

    void WS2812B::flush() {
        if (flushed) {
            return;
        }

        // can keep this in memory
        static std::vector<std::uint8_t> data;
        data.resize(config.leds * BYTE_PER_LED);

        // convert colors to the data to be sent through SPI
        for (std::size_t i = 0; i < state.size(); ++i) {
            setData(data, i, state[i]);
        }

        config.spi->send(data);
        flushed = true;
    }

    void WS2812B::setColor(int index, common::Color color) {
        if (color != state[index]) {
            state[index] = color;
            flushed = false;
        }

        if (config.flushMode == FlushMode::AUTO) {
            flush();
        }
    }

    void WS2812B::setColor(std::vector<std::pair<std::size_t, common::Color>> colors) {
        for (const auto& [index, color] : colors) {
            if (color != state[index]) {
                state[index] = color;
                flushed = false;
            }
        }

        if (config.flushMode == FlushMode::AUTO) {
            flush();
        }
    }

    void WS2812B::setColor(std::vector<common::Color> colors) {
        if (colors.size() > state.size()) {
            throw pizzy::common::Exception("Given more colors (" + std::to_string(colors.size()) + ") than the device \"" + getName() +
                                           "\" has LEDs");
        }
        for (std::size_t index = 0; index < colors.size(); ++index) {
            if (colors[index] != state[index]) {
                state[index] = colors[index];
                flushed = false;
            }
        }

        if (config.flushMode == FlushMode::AUTO) {
            flush();
        }
    }

    bool WS2812B::isFlushed() const {
        return flushed;
    }

} // namespace pizzy::peripherals
