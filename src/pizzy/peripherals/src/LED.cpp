#include <pizzy/common/Exception.h>
#include <pizzy/core/Pin.h>
#include <pizzy/peripherals/LED.h>

#include <string>

namespace {
    std::string typeToString(const pizzy::peripherals::LEDType type) {
        switch (type) {
        case pizzy::peripherals::LEDType::CATHODE:
            return "cathode";
        case pizzy::peripherals::LEDType::ANODE:
            return "anode";
        default:
            return "unknown";
        }
    }
} // namespace

namespace pizzy::peripherals {

    LED::LED(LEDConfig&& conf)
        : Peripheral(typeToString(conf.type) + "-LED at " + conf.pin->getName())
        , config(std::move(conf)) {
        if (typeToString(this->config.type) == "unknown") {
            throw common::Exception("LED Type was 'unknown', can't create LED " + getName());
        }

        setState(config.defaultState);
    }

    LED::~LED() {
        setState(config.defaultState);
    }

    void LED::setState(const bool state) {
        switch (config.type) {
        case LEDType::ANODE:
            config.pin->write(state);
            break;
        case LEDType::CATHODE:
            config.pin->write(!state);
            break;
        }
        this->state = state;
    }

    bool LED::getState() const {
        return state;
    }

    std::string LED::toString() const {
        return getName() + " turned " + (state ? "on" : "off");
    }

} // namespace pizzy::peripherals
