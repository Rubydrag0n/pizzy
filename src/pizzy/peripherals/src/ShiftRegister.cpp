#include <pizzy/common/Exception.h>
#include <pizzy/common/Utility.h>
#include <pizzy/core/Pin.h>
#include <pizzy/core/PinProvider.h>
#include <pizzy/core/Provider.h>
#include <pizzy/peripherals/ShiftRegister.h>

using namespace pizzy::core;

namespace pizzy::peripherals {

    ShiftRegister::ShiftRegister(ShiftRegisterConfig&& config)
        : Provider("Shift Register", "SR")
        , config(std::move(config)) {
        if (this->config.data == nullptr) {
            throw common::Exception("Must provide data pin for a ShiftRegister");
        }
        if (this->config.clock == nullptr) {
            throw common::Exception("Must provide clock pin for a ShiftRegister");
        }
        if (this->config.latch == nullptr) {
            throw common::Exception("Must provide latch pin for a ShiftRegister");
        }

        if (this->config.size == 0) {
            throw common::Exception("Invalid ShiftRegister size 0");
        }

        if (this->config.enable != nullptr) {
            this->config.enable->write(false);
        }
        if (this->config.clear != nullptr) {
            this->config.clear->write(true);
        }

        registerValues.resize(static_cast<std::size_t>(this->config.size), false);
        clear();
    }

    ShiftRegister::~ShiftRegister() {
    }

    std::unique_ptr<Pin> ShiftRegister::getPin(const int pinNumber) {
        auto pin = PinProvider::createPin(pinNumber);

        return pin;
    }

    void ShiftRegister::writePin(const int pinNumber, const bool state) {
        registerValues[pinNumber] = state;
        set();
    }

    void ShiftRegister::bulkFlush(const std::vector<std::pair<int, bool>>& ops) {
        for (const auto& op : ops) {
            registerValues[op.first] = op.second;
        }
        set();
    }

    bool ShiftRegister::readPin(const int pinNumber) {
        throw common::Exception("Attempting to read a pin on " + getName() + ", pinNumber " + std::to_string(pinNumber));
    }

    void ShiftRegister::deletePin(const int /*pinNumber*/) {
    }

    void ShiftRegister::clear() {
        std::fill(registerValues.begin(), registerValues.end(), false);

        if (config.clear != nullptr) {
            // pull low to clear
            config.clear->write(false);

            // shift registers are very fast
            common::Utility::sleepMicroseconds(0);

            // pull high again
            config.clear->write(true);
        } else {
            set();
        }
    }

    void ShiftRegister::set() {
        for (const auto& v : registerValues) {
            config.data->write(v);

            config.clock->write(true);
            common::Utility::sleepMicroseconds(0);
            config.clock->write(false);
        }

        // reset pin off
        config.data->write(false);

        publish();
    }

    void ShiftRegister::publish() {
        config.latch->write(true);
        common::Utility::sleepMicroseconds(0);
        config.latch->write(false);
    }

} // namespace pizzy::peripherals
