#include <pizzy/common/Color.h>
#include <pizzy/common/Exception.h>

#include <cmath>

namespace pizzy::common {

    Color Color::fromRGB(const std::uint8_t r, const std::uint8_t g, const std::uint8_t b) {
        return Color{.r = r, .g = g, .b = b};
    }

    Color Color::fromHSV(const float h, const float s, const float v) {
        if (h > 1 || h < 0) {
            throw pizzy::common::Exception("Trying to construct a color with hue out of range (must be between 0 and 1)");
        }
        if (s > 1 || s < 0) {
            throw pizzy::common::Exception("Trying to construct a color with saturation out of range (must be between 0 and 1)");
        }
        if (v > 1 || v < 0) {
            throw pizzy::common::Exception("Trying to construct a color with value out of range (must be between 0 and 1)");
        }

        float c = v * s;
        float x = c * (1 - std::abs(std::fmod(h * 6, 2) - 1));
        float m = v - c;
        c += m;
        x += m;
        std::uint8_t C = static_cast<std::uint8_t>(c * 255);
        std::uint8_t X = static_cast<std::uint8_t>(x * 255);
        std::uint8_t M = static_cast<std::uint8_t>(m * 255);

        std::uint8_t r = 0, g = 0, b = 0;
        switch (static_cast<int>(h * 6)) {
        case 0:
            [[fallthrough]];
        case 6:
            r = C;
            g = X;
            b = M;
            break;
        case 1:
            r = X;
            g = C;
            b = M;
            break;
        case 2:
            r = M;
            g = C;
            b = X;
            break;
        case 3:
            r = M;
            g = X;
            b = C;
            break;
        case 4:
            r = X;
            g = M;
            b = C;
            break;
        case 5:
            r = C;
            g = M;
            b = X;
            break;
        default:
            throw pizzy::common::Exception("Impossible");
        }

        return Color{.r = r, .g = g, .b = b};
    }

#if __cplusplus < CPP20
    bool Color::operator==(const Color& c) const noexcept {
        return r == c.r && g == c.g && b == c.b;
    }

    bool Color::operator!=(const Color& c) const noexcept {
        return !(*this == c);
    }
#endif

} // namespace pizzy::common
