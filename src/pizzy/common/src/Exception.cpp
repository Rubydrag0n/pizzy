#include <pizzy/common/Exception.h>

namespace pizzy::common {

    Exception::Exception(std::string message)
        : message(std::move(message)) {
    }

    const char* Exception::what() const noexcept {
        return message.c_str();
    }

    std::ostream& operator<<(std::ostream& os, Exception exception) {
        os << exception.what();
        return os;
    }

} // namespace pizzy::common
