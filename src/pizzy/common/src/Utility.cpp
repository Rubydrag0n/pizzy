#include <pizzy/common/Utility.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <string>
#include <thread>
#include <array>

#include <sched.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>

#include <sched.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>

namespace pizzy::common {

    std::string Utility::getVersion() {
        return common::PIZZY_VERSION;
    }

    std::string Utility::timeToString(const clock::duration time) {
        std::ostringstream ss;
        ss << std::setfill('0');

        const auto hrs = std::chrono::duration_cast<std::chrono::hours>(time);
        const auto mins = std::chrono::duration_cast<std::chrono::minutes>(time - hrs);
        const auto secs = std::chrono::duration_cast<std::chrono::seconds>(time - hrs - mins);
        const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(time - hrs - mins - secs);
        const auto us = std::chrono::duration_cast<std::chrono::microseconds>(time - hrs - mins - secs - ms);
        const auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(time - hrs - mins - secs - ms - us);

        if (hrs.count() > 0) {
            ss << hrs.count() << ":" << std::setw(2) << mins.count() << ":" << std::setw(2) << secs.count();
        } else if (mins.count() > 0) {
            ss << mins.count() << ":" << std::setw(2) << secs.count() << "." << std::setw(3) << ms.count();
        } else if (secs.count() > 0) {
            ss << secs.count() << "." << std::setw(3) << ms.count() << "s";
        } else if (ms.count() > 0) {
            ss << ms.count() << "." << std::setw(3) << us.count() << "ms";
        } else {
            ss << us.count() << "." << std::setw(3) << ns.count() << "us";
        }

        return ss.str();
    }

    void Utility::sleepMicroseconds(const int microseconds) {
        std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    }

    void Utility::sleepMicroseconds(const double microseconds) {
        const auto sleepDuration = std::chrono::nanoseconds(int(microseconds * 1000));
        std::this_thread::sleep_for(sleepDuration);
    }

    void Utility::sleepMilliseconds(const int milliseconds) {
        std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
    }

    void Utility::sleepMilliseconds(const double milliseconds) {
        const auto sleepDuration = std::chrono::nanoseconds(int(milliseconds * 1000000));
        std::this_thread::sleep_for(sleepDuration);
    }

    void Utility::sleepSeconds(const int seconds) {
        std::this_thread::sleep_for(std::chrono::seconds(seconds));
    }

    void Utility::sleepSeconds(const double seconds) {
        const auto sleepDuration = std::chrono::nanoseconds(uint64_t(seconds * 1000000000ULL));
        std::this_thread::sleep_for(sleepDuration);
    }

    bool Utility::fileExists(const std::string& path) {
        return std::filesystem::exists(path);
    }

    std::string Utility::readFile(const std::string& path) {
        if (!fileExists(path)) {
            return "";
        }

        std::stringstream content;
        std::fstream file(path);
        content << file.rdbuf();
        std::string res = content.str();
        if (!res.empty()) {
            res.pop_back();
        }
        return res;
    }

    std::string Utility::getSystemCallOutput(const std::string& command) {
        std::stringstream ss;
        std::array<char, 128> buffer{};

        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command.c_str(), "r"), pclose);
        if (!pipe) {
            // couldn't open pipe
            return "";
        }

        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            ss << buffer.data();
        }

        std::string res = ss.str();
        if (!res.empty()) {
            res.pop_back();
        }
        return res;
    }

} // namespace pizzy::common
