/** @file */
#pragma once

#include <exception>
#include <sstream>
#include <string>

namespace pizzy::common {

    /**
     * @brief pizzy's base exception type. Every exception thrown by the library will have this base type
     */
    class Exception : public std::exception {
    public:
        /**
         * @brief Creates an exception with the given message
         */
        Exception(std::string message);

        /**
         * @brief Returns the message that the exception was created with
         * @return The message
         */
        const char* what() const noexcept override;

    protected:
    private:
        std::string message;
    };

    /**
     * @brief ostream pipe overload for easy debugging with std::cout or other ostreams
     * @details Will pipe the toString() into the given ostream
     */
    std::ostream& operator<<(std::ostream& os, const Exception& exception);

} // namespace pizzy::common
