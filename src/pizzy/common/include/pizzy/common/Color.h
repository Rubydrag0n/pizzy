/** @file */
#pragma once

#include <pizzy/common/definitions.h>

#include <cmath>

#if __cplusplus >= CPP20
#include <compare>
#endif

namespace pizzy::common {

    /**
     * @brief A class representing a color. Internally represented as rgb 8bit values, but can be constructed from other color spaces as well.
     */
    struct Color {
    public:
        /**
         * @brief Create a color object from r, g and b values from 0 - 255
         *
         * @param r The red part of the color, from 0 to 255
         * @param g The green part of the color, from 0 to 255
         * @param b The blue part of the color, from 0 to 255
         * @return The resulting Color
         */
        [[nodiscard]] static Color fromRGB(std::uint8_t r, std::uint8_t g, std::uint8_t b);

        /**
         * @brief Create a color object from h, s and v values from 0 - 1.0
         *
         * @param h The hue of the color, from 0 to 1
         * @param s The saturation of the color, from 0 to 1
         * @param v The value of the color, from 0 to 1
         * @return The resulting Color
         */
        [[nodiscard]] static Color fromHSV(float h, float s, float v);

#if __cplusplus >= CPP20
        /**
         * @brief Spaceship operator for comparisons
         * @details Read this very epic article as to why it's so epic
         * https://devblogs.microsoft.com/cppblog/simplify-your-code-with-rocket-science-c20s-spaceship-operator/
         */
        [[nodiscard]] auto operator<=>(const Color& c) const = default;
#else
        /**
         * @brief Equality operator for comparisons
         */
        [[nodiscard]] bool operator==(const Color& c) const noexcept;

        /**
         * @brief Inequality operator for comparisons
         */
        [[nodiscard]] bool operator!=(const Color& c) const noexcept;
#endif

        /**
         * @brief The red color part
         */
        std::uint8_t r;

        /**
         * @brief The green color part
         */
        std::uint8_t g;

        /**
         * @brief The blue color part
         */
        std::uint8_t b;

    protected:
    private:
    };

} // namespace pizzy::common
