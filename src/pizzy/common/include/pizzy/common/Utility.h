/** @file */
#pragma once

#include <pizzy/common/definitions.h>

#include <chrono>
#include <future>
#include <sstream>
#include <string>
#include <vector>

namespace pizzy::common {

    /**
     * @brief The chrono clock used throughout the project
     */
    using clock = std::chrono::steady_clock;

    /**
     * @brief Custom literal operator for uint8_t
     */
    inline constexpr std::uint8_t operator"" _u8(unsigned long long arg) noexcept {
        return static_cast<std::uint8_t>(arg);
    }

    /**
     * @brief Collection of static utility functions
     */
    class Utility {
    public:
        /**
         * @brief Returns the Version String
         */
        static std::string getVersion();

        /**
         * @brief Takes a duration and converts it to a human readable string (always rounds down)
         *
         * @param time A chrono duration that will be converted to a string
         * @return The resulting string
         */
        static std::string timeToString(const clock::duration time);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepMicroseconds(int microseconds);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepMicroseconds(double microseconds);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepMilliseconds(int milliseconds);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepMilliseconds(double milliseconds);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepSeconds(int seconds);

        /**
         * @brief Sleeps for the given duration
         */
        static void sleepSeconds(double seconds);

        /**
         * @brief Checks if a file or directory exists
         *
         * @param path The filepath to check, absolute or relative to where the program was started from
         * @return True if the file exists
         */
        static bool fileExists(const std::string& path);

        /**
         * @brief Reads the entire content of a file and returns it
         * @details If the given file does not exist or can't be read for other reasons, an empty string will be returned
         *
         * @param path The path to the file to read
         * @return The content of the file as a string
         */
        static std::string readFile(const std::string& path);

        /**
         * @brief executes a command in the shell and returns the output as a string
         */
        static std::string getSystemCallOutput(const std::string& command);

    protected:
    private:
    };

} // namespace pizzy::common
