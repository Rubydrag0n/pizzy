#include <pizzy/common/Color.h>
#include <pizzy/common/Exception.h>
#include <pizzy/common/Utility.h>
#include <pizzy/core/BCM2835.h>
#include <pizzy/peripherals/WS2812B.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;
using namespace pizzy::peripherals;
using namespace pizzy::core;
using namespace pizzy::common;

constexpr uint32_t FRAMERATE = 60;
constexpr uint32_t PULSE_DURATION = 2;
constexpr std::size_t NUMBER_OF_LEDS = 12;

/**
 * @brief For this demo setup your LED Ring or strip to have the IN pin on the SPI MOSI pin, which is GPIO10, or physical pin 19
 * Connect VCC to 3.3V and GND to the RPI GND.
 * If you want to use VCC of 5V you need to use a voltage transformer for the Data pin as well
 */
int main(int, char**) {
    try {
        auto& board = BCM2835::getInstance();

        WS2812B device(WS2812BConfig{.spi = board.getSPI(0), .leds = NUMBER_OF_LEDS, .flushMode = FlushMode::MANUAL});
        std::cout << "Created WS2812B Device: " << device.toString() << std::endl;

        bool run = true;

        std::thread t([&device, &run]() {
            unsigned count = 0;
            while (run) {
                count++;

                float v = std::sin(count * 2. * 3.1416 / (FRAMERATE * PULSE_DURATION)) * 0.2 + 0.19;
                float s = 1.;
                float h = std::fmod((count * 2) / 360., 1.);
                for (std::size_t led = 0; led < NUMBER_OF_LEDS; ++led) {
                    device.setColor(led, Color::fromHSV(h, s, v > 0 ? v : 0));
                    h = std::fmod(h + 1. / NUMBER_OF_LEDS, 1.);
                }

                device.flush();
                pizzy::common::Utility::sleepMilliseconds(1000. / FRAMERATE);
            }
        });

        std::cout << "LED Ring should be blinking" << std::endl;
        std::cout << "Press return to continue...";
        // wait for enter press
        std::cin.get();

        run = false;

        std::cout << "Waiting for thread to return..." << std::endl;
        if (t.joinable()) {
            t.join();
        }

    } catch (const pizzy::common::Exception& e) {
        std::cout << "Caught pizzy exception: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cout << "Caught unknown exception" << std::endl;
        return 1;
    }

    std::cout << "Demo successful!" << std::endl;
    return 0;
}
