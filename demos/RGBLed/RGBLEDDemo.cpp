#include <pizzy/common/Color.h>
#include <pizzy/common/Exception.h>
#include <pizzy/core/BCM2835.h>
#include <pizzy/peripherals/RGBLED.h>

#include <chrono>
#include <iostream>
#include <thread>

using namespace pizzy::common;
using namespace pizzy::core;
using namespace pizzy::peripherals;
using namespace std::chrono_literals;

/**
 * @brief For this demo setup your LED on pins 14 (red), 15 (green) and 18 (blue) (the serial connection pins)
 * A common-cathode LED should be used, but a common-anode LED will work too, the colors will just be inverted
 */
int main(int, char**) {
    // catch any potential exceptions
    try {
        auto& bcm = BCM2835::getInstance();

        RGBLED rgbled(RGBLEDConfig{.red = bcm.getPin(14), .green = bcm.getPin(15), .blue = bcm.getPin(18), .type = RGBLEDTYPE::COMMON_CATHODE});

        bool run = true;
        std::thread t([&rgbled, &run]() {
            while (run) {
                rgbled.set(COLOR::RED);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::GREEN);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::BLUE);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::YELLOW);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::TURQUOISE);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::PURPLE);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::WHITE);
                std::this_thread::sleep_for(100ms);
                rgbled.set(COLOR::BLACK);
                std::this_thread::sleep_for(500ms);
            }
        });

        std::cout << "LED should be blinking" << std::endl;
        std::cout << "Press return to continue...";
        // wait for enter press
        std::cin.get();

        run = false;

        std::cout << "Waiting for thread to return..." << std::endl;
        if (t.joinable()) {
            t.join();
        }

    } catch (const pizzy::common::Exception& e) {
        std::cout << "Caught pizzy exception: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cout << "Caught unknown exception" << std::endl;
        return 1;
    }

    std::cout << "Demo successful!" << std::endl;
    return 0;
}
