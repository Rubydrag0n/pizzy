#include <pizzy/common/Exception.h>
#include <pizzy/common/Utility.h>
#include <pizzy/core/BCM2835.h>
#include <pizzy/peripherals/LED.h>
#include <pizzy/peripherals/ShiftRegister.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;
using namespace pizzy::peripherals;
using namespace pizzy::core;
using namespace pizzy::common;

constexpr uint32_t FRAMERATE = 30;
constexpr uint32_t SHIFTREGSIZE = 8;

/**
 * @brief Simple class to control multiple LEDs on a ShiftRegister together, making use of bulkWrites
 */
class LEDSet {
public:
    LEDSet()
        : reg(ShiftRegisterConfig{.data = BCM2835::getInstance().getPin(14),
                                  .clock = BCM2835::getInstance().getPin(15),
                                  .latch = BCM2835::getInstance().getPin(18),
                                  .size = SHIFTREGSIZE}) {
        // create one LED on every output
        for (std::uint32_t i = 0; i < SHIFTREGSIZE; ++i) {
            leds.emplace_back(reg.getPin(i));
        }
    }

    ~LEDSet() {
        // turn leds off at the end
        for (auto& pin : leds) {
            pin->write(false);
        }
    }

    void setPins(const std::vector<std::pair<int, bool>>& ops) {
        for (const auto& op : ops) {
            leds[op.first]->bulkWrite(op.second);
        }

        Pin::bulkFlush();
    }

private:
    ShiftRegister reg;

    std::vector<std::unique_ptr<Pin>> leds;
};

/**
 * @brief For this demo, setup your ShiftRegister with VCC connected to 3.3V, GND to a ground pin, the data pin to GPIO14, the clock pin to GPIO15 and
 * the latch pin to GPIO18. Connect 8 LEDs to the outputs of the shift register (+ side to the shift register output pins, - side to GND). Use
 * resistors if the LEDs can't take 3.3V.
 * If you want to use VCC of 5V you need to use a voltage transformer for the Data pin as well.
 */
int main(int, char**) {
    try {
        bool run = true;

        std::thread t([&run]() {
            LEDSet ledSet;
            std::vector<std::pair<int, bool>> leds;
            leds.resize(SHIFTREGSIZE);
            unsigned count = 0;
            while (run) {
                if (count < SHIFTREGSIZE) {
                    leds.emplace_back(std::pair<int, bool>(count, true));
                } else {
                    leds.emplace_back(std::pair<int, bool>(count - SHIFTREGSIZE, false));
                }

                ledSet.setPins(leds);
                leds.clear();

                count = (count + 1) % (SHIFTREGSIZE * 2);

                pizzy::common::Utility::sleepMilliseconds(1000. / FRAMERATE);
            }
        });

        std::cout << "LEDs should be blinking" << std::endl;
        std::cout << "Press return to continue...";
        // wait for enter press
        std::cin.get();

        run = false;

        std::cout << "Waiting for thread to return..." << std::endl;
        if (t.joinable()) {
            t.join();
        }

    } catch (const pizzy::common::Exception& e) {
        std::cout << "Caught pizzy exception: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cout << "Caught unknown exception" << std::endl;
        return 1;
    }

    std::cout << "Demo successful!" << std::endl;
    return 0;
}
