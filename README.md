# Pi Peripherals

A modern C++ library for using the GPIOs of a Raspberry Pi (and potentially other boards in the future) to control peripherals like LEDs, Servos, Relays etc.

This is still in (early) development, stuff can (and will) change. Bugs can (and will) happen. I take no responsibility if you damage anything using this library.

## Usage

You can clone the repository and install the project (see build instructions), or add the project via a git submodule and use cmake.

For example:
```cmake
set(PIZZY_BUILD_TESTS OFF CACHE BOOL "Build tests" FORCE)
set(PIZZY_BUILD_DEMOS OFF CACHE BOOL "Build demos" FORCE)
add_subdirectory(pizzy)
```

Then you can link against all the pizzy targets and cmake does the rest.

## Build Instructions

You can install this on a Raspberry PI to use it or install on a different platform to mock stuff.

Prerequisites:
- bcm2835 library, install e.g. `trizen -S bcm2835`
- standard C++ build stuff: gcc/clang/compiler of choice, make/ninja/build system of choice, cmake, optionally ccache

Clone the repository and cd into it, then
```sh
$ mkdir build && cd build
$ cmake .. -DCMAKE_INSTALL_PREFIX=../install
$ cmake --build . --target install
```
to install into a directory next to the build directory. To install globally in your system, omit the `-DCMAKE_INSTALL_PREFIX=...` argument and use sudo to build.

To run tests you can use
```sh
$ cmake --build . --target check
```

## Current State

Supported boards:
- BCM2835 (i.e. any raspberry pi should work, tested only on rpi 4b so far)

Supported interfaces
- Pin out
- SPI

Supported peripherals:
- LED
- RGB LED
- Relay
- WS2812B Devices (LED strips and such)

## General Goals

Easy and safe to use library for people familiar with modern C++. Support as many peripherals as possible and some electronic parts like shift registers.
Build system will be CMake based, Unit Test framework will be catch2

List of planned peripherals:

- LEDs
- Servos (analog + digital)
- RelayFeature/f
- Object Oriented Design: Create an object (e.g. a LED) at a given PIN, then tell it what to do via member functions
- RAII: No init functions for any of the classes
- It should be hard to shoot yourself in the foot:
    * No init functions you have to call on startup or teardown at the end, should be done statically by the library
    * Impossible to use the same Pin for multiple things
- High level abstractions: No manual setting of pin values (unless you want to), instead the library provides useful functions for the type of peripheral connected to a given Pin or interface
- Error handling is consistently done using exceptions, no success return codes or out-parameter error variables
- Thread safety
- Good test coverage using automated Unit Tests (catch2) and Mocks for the peripherals
- Ensure code quality by enabling all warnings and -Werror, using clang-format and clang-tidy, and static analyzers like asan and tsan
- Configurable logging for debugging
- Cross compilation should be possible, possibly also a mockup mode for test execution
- Generate doxygen documentation

## Specifics

For the full documentation see [doxygen](https://rubydrag0n.gitlab.io/pizzy/index.html)

Internal foundation classes

### PeripheralRegistry
    * A registry that knows about all available GPIOs and what peripherals are registered for them
    * Can be asked about peripherals registered on a given pin

### Peripheral
    * Base class for all other peripherals
    * Constructed with given pin(s), taking ownership of it, registering itself in the PeripheralRegistry
    * Has a name (for logging and identification)
    * Can own multiple Pins, for example a LED matrix

### PinProvider
    * A provider for pins, the base one is just the board's own hat
    * Provides the interface for setting its physical pins' values

### Pin
    * Provides basic abstractions of a single GPIO
    * Provides functionality such as setting the Pin to High or Low, setting an interrupt callback, setting the Pin's mode and such
    * Also support something like a "virtual" pin when used through a multiplexer or similar

### PeripheralConfig
    * Base config, in best case every config is read/writable from files

For each individual peripheral:

### The class itself
    * Provide abstractions relevant to the peripheral (Servos can set an angle, LEDs could set a brightness)
    * Declared final

### Configuration Struct
    * A configuration that collects the info to construct a peripheral
    * Overloads a write and read function to file or string
    * Should give nice syntax with c++20 (or gcc extension) like `LED myLed{.pin = 5, .name = "My Led"}`
